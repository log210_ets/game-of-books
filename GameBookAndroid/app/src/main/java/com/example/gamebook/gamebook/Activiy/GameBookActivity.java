package com.example.gamebook.gamebook.Activiy;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gamebook.gamebook.Activiy.Manager.ManagerActivity;
import com.example.gamebook.gamebook.Activiy.User.UserMainActivity;
import com.example.gamebook.gamebook.Model.User;
import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;

public class GameBookActivity extends AppCompatActivity {
    GobService _service;
    EditText _textPassword;
    EditText _textEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_book);

        _service = GobService.getInstance();

        _textPassword = (EditText) findViewById(R.id.input_password);
        _textEmail = (EditText) findViewById(R.id.input_email);


        String token = PreferenceManager.getDefaultSharedPreferences(this).getString("Token", "");
        if(token.equals(""))
            return;;
        User user = _service.user().GetLoggedUser(token);

        if(user.getIsAuthentified())
            AuthentifyUser(user);
    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    private  void AuthentifyUser(User user){
        _service.setLogUser(user);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("Token", user.getSessionToken()).apply();
        OpenActivityByUserRole(user);
    }

    private  void  OpenActivityByUserRole(User user){

        String role = user.getRoles()[0];


        if(role.equals("Student") || role.equals("Dev") )
        {
            Intent intent = new Intent(this, UserMainActivity.class);
            startActivity(intent);
            finish();
        }
        else if (role.equals("Manager"))
        {
            Intent intent = new Intent(this, ManagerActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            Toast bread = Toast.makeText(getApplicationContext(),"Account has no role.", Toast.LENGTH_LONG);
            bread.show();
        }
    }

    public void OnLoginClick(View view) {

        // Remove the keyboard form the view
        view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }


        if (!validate()) {
            return;
        }

        EditText _textPassword = (EditText) findViewById(R.id.input_password);
        EditText _textEmail = (EditText) findViewById(R.id.input_email);

        User user = new User();
        user.setEmail(_textEmail.getText().toString());
        user.setPassword(_textPassword.getText().toString());
        user = _service.user().Login(user);

        if(user.getIsAuthentified())
        {
            Toast bread = Toast.makeText(getApplicationContext(),"Success!", Toast.LENGTH_LONG);
            bread.show();
            AuthentifyUser(user);
        }

        else
        {
            Toast bread = Toast.makeText(getApplicationContext(),"Invalid Credential", Toast.LENGTH_LONG);
            bread.show();
        }
    }

    public void onSignUp(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ui.3am-eternal.org/#/login"));
        startActivity(browserIntent);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _textEmail.getText().toString();
        String password = _textPassword.getText().toString();

        if (email.isEmpty()) {
            _textEmail.setError("enter a valid email address");
            valid = false;
        } else {
            _textEmail.setError(null);
        }

        if (password.isEmpty()) {
            _textPassword.setError("enter a valid password");
            valid = false;
        } else {
            _textPassword.setError(null);
        }

        return valid;
    }
}
