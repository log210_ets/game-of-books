package com.example.gamebook.gamebook.Model;

/**
 * Created by Julien on 11/16/2016.
 */

public class BookCopy {

    private BookCondition Condition;
    private  BookInfo BookDescription;

    public BookCondition getCondition() {
        return Condition;
    }

    public void setCondition(BookCondition condition) {
        Condition = condition;
    }

    public BookInfo getBookDescription() {
        return BookDescription;
    }

    public void setBookDescription(BookInfo bookDescription) {
        BookDescription = bookDescription;
    }
}
