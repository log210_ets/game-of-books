package com.example.gamebook.gamebook.Service.Services;

import com.example.gamebook.gamebook.Model.User;

/**
 * Created by Julien on 11/12/2016.
 */

public class GobService {


    private User logUser;

    private UserService user = new UserService();
    private BookService book = new BookService();
    private BookConditionService bookCondition = new BookConditionService();



    private static GobService _instance = null;

    private GobService() {
        //Singleton
    }

    public static GobService getInstance() {
        if(_instance == null) {
            _instance = new GobService();
        }
        return _instance;
    }



    // TODO GÉRÉ le TOKEN




    public UserService user() {
        return user;
    }
    public BookService book() {
        return book;
    }
    public BookConditionService bookCondition() {
        return bookCondition;
    }

    public User getLogUser() {
        return logUser;
    }
    public void setLogUser(User logUser) {
        this.logUser = logUser;
    }
}
