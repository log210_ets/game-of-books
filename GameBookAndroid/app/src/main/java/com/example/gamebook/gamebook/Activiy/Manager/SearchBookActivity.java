package com.example.gamebook.gamebook.Activiy.Manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.example.gamebook.gamebook.Activiy.Adapter.ItemAdapter;
import com.example.gamebook.gamebook.Activiy.User.UserMenu;
import com.example.gamebook.gamebook.Model.BookCondition;
import com.example.gamebook.gamebook.Model.BookInfo;
import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;

import java.util.ArrayList;
import java.util.List;

public class SearchBookActivity extends UserMenu implements AdapterView.OnItemClickListener {
    private FloatingSearchView mSearchView;
    GobService _service;
    String _action;
    ItemAdapter fAdapter;
    ListView fList;
    BookCondition[] _conditions;
    String _searchValue;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        _service = GobService.getInstance();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(_action + " A Book");
        }

        return true;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_book);

        _action = getIntent().getStringExtra(EXTRA_MESSAGE);

        _service = GobService.getInstance();
        _conditions = _service.bookCondition().getBookCondition();

        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);

        RelativeLayout view = (RelativeLayout) findViewById(R.id.content_search_book);
        view.setVisibility(View.INVISIBLE);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        List<BookInfo> lst = new ArrayList<BookInfo>();

        fList = (ListView) findViewById(R.id.booksListView);
        fAdapter = new ItemAdapter(this, lst);
        fList.setAdapter(fAdapter);
        fList.setOnItemClickListener(this);

        registerForContextMenu(fList);

        setupFloatingSearch();

        String search = getIntent().getStringExtra("search");
        if(search != null)
            search(search);
    }

    private void setupFloatingSearch() {
        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {


            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String query) {

                if (query.trim().equals(""))
                    return;

                search(query);
            }
        });
    }

    private  void search(String query){
        _searchValue = query;

        List<BookInfo> lst = new ArrayList<BookInfo>();

        if(_action.equals("Recover"))
        {
            lst = _service.book().searchBookToRecover(query);
        }else if (_action.equals("Receive"))
        {
            lst = _service.book().searchBookToReceive(query);
        }

        RelativeLayout view = (RelativeLayout) findViewById(R.id.content_search_book);
        view.setVisibility(View.VISIBLE);

        fAdapter.updateList(lst);
        fAdapter.notifyDataSetChanged();
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        BookInfo bookInfoClick =  fAdapter.getItem(position);

        Intent intent = new Intent(this, ReceiveOrRecoverActivity.class);
        intent.putExtra("BookInfo", bookInfoClick);
        intent.putExtra("ConditionInfo", _conditions);
        intent.putExtra("ReceiveOrRecover", _action);
        intent.putExtra("search", _searchValue);

        startActivity(intent);
    }

}
