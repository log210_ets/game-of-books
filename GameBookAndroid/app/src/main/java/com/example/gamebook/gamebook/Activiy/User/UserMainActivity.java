package com.example.gamebook.gamebook.Activiy.User;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class UserMainActivity extends UserMenu {


    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionBarCode, floatingActionButton2;


    GobService _service;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        _service = GobService.getInstance();
        final Activity activity = this;

        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionBarCode = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item2);

        floatingActionBarCode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(activity);
                scanIntegrator.initiateScan();

            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openAddBookActivity(null);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        Activity activity = this;

        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            // String scanFormat = scanningResult.getFormatName();

            if(scanContent != null)
               openAddBookActivity(scanContent);

        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void openAddBookActivity(String searchValue) {
        Intent intent = new Intent(this, AddBookActivity.class);

        if (searchValue != null) {
            String message = searchValue;
            intent.putExtra(EXTRA_MESSAGE, message);
        }

        startActivity(intent);
    }
}
