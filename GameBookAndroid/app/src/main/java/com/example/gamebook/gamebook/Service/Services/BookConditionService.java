package com.example.gamebook.gamebook.Service.Services;

import com.example.gamebook.gamebook.Model.BookCondition;
import com.google.gson.Gson;

import org.springframework.http.ResponseEntity;

/**
 * Created by Julien on 11/14/2016.
 */

public class BookConditionService extends Service{ {


}
    public BookConditionService()
    {
        super("BookCondition/");
    }

    public BookCondition[] getBookCondition(){
        ResponseEntity responseEntity = _http.get(GetUri(""),GetToken());

        // conversion
         BookCondition[] bookConditions = new Gson().fromJson(responseEntity.getBody().toString(),BookCondition[].class);

     return  bookConditions;
    }
}
