package com.example.gamebook.gamebook.Service;

import org.springframework.http.HttpMethod;

/**
 * Created by Julien on 11/12/2016.
 */

public class TemplateRequest {


    private String _uri;
    private String _token;
    private HttpMethod _httpMethod;
    private Object _request;


    public TemplateRequest(String uri, String token, HttpMethod httpMethod, Object request) {
        _uri = uri;
        _token = token;
        _httpMethod = httpMethod;
        _request = request;
    }

    public String getUri() {
        return _uri;
    }

    public String getToken() {
        return _token;
    }

    public HttpMethod getHttpMethod() {
        return _httpMethod;
    }

    public Object getRequest() {
        return _request;
    }
}

