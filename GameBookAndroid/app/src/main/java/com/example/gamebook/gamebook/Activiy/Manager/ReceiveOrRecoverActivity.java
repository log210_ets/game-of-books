package com.example.gamebook.gamebook.Activiy.Manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.gamebook.gamebook.Activiy.User.UserMenu;
import com.example.gamebook.gamebook.Model.BookCondition;
import com.example.gamebook.gamebook.Model.BookInfo;
import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;

public class ReceiveOrRecoverActivity extends UserMenu {

    BookCondition[] _conditions;
    BookInfo _bookInfo;
    String _action;
    EditText _ISNB10, _EAN, _UPC, _title, _author, _pages, _price, _coopPrice, _sellingPrice, _userEmail;
    Spinner _condition;
    Button _sendButton, _cancelButton;
    String _searchValue;
    GobService _service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_or_recover);
        _service = GobService.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        _conditions = (BookCondition[]) getIntent().getSerializableExtra("ConditionInfo");
        _bookInfo = (BookInfo) getIntent().getSerializableExtra("BookInfo");
        _action = getIntent().getStringExtra("ReceiveOrRecover");
        _searchValue = getIntent().getStringExtra("search");

        initializeComponent();
        populateField(_bookInfo.getISNB10(),
                _bookInfo.getEAN(),
                _bookInfo.getUPC(),
                _bookInfo.getTitle(),
                _bookInfo.getAuthor(),
                String.valueOf(_bookInfo.getNbPage()),
                _bookInfo.getDefaultPrice().toString(),
                _bookInfo.getCooperativePrice().toString(),
                _bookInfo.getSellingPrice().toString(),
                _bookInfo.getUserEmail(),
                _bookInfo.getBookCondition());


        _sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onConfirm();
            }

        });
        _cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onCancel();
            }

        });
    }

    private void initializeComponent() {


        String[] _bookConditions = new String[_conditions.length];

        for (int i = 0; i < _conditions.length; i++) {
            _bookConditions[i] = _conditions[i].getBookCondition();
        }


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, _bookConditions);
        _condition = (Spinner)
                findViewById(R.id.input_Condition2);
        _condition.setAdapter(arrayAdapter);


        _ISNB10 = (EditText) findViewById(R.id.input_ISNB10);
        _EAN = (EditText) findViewById(R.id.input_EAN);
        _UPC = (EditText) findViewById(R.id.input_UPC);
        _title = (EditText) findViewById(R.id.input_Title);
        _author = (EditText) findViewById(R.id.input_Author);
        _pages = (EditText) findViewById(R.id.input_pages);
        _price = (EditText) findViewById(R.id.input_Price);
        _coopPrice = (EditText) findViewById(R.id.input_CoopPrice);
        _sellingPrice = (EditText) findViewById(R.id.input_SellingPrice);
        _userEmail = (EditText) findViewById(R.id.input_UserEmail);
        _sendButton = (Button) findViewById(R.id.btn_confirm);
        _cancelButton = (Button) findViewById(R.id.btn_cancel);

       if(_action.equals("Receive"))
       {
           _coopPrice.setFocusableInTouchMode(true);
           _coopPrice.setFocusable(true);
       }
        else
       {
           _condition.setEnabled(false);
       }

    }



    private BookCondition findCondition(String conditon) {
        for (int i = 0; i < _conditions.length; i++) {
            if (_conditions[i].getBookCondition().equals(conditon))
                return _conditions[i];

        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(_action + " A Book");
        }

        return true;
    }
    private void populateField(String ISBN10, String EAN, String UPC, String title, String author, String pages, String price,
                               String coopPrice,String sellingPrice, String userEmail,String condition) {

        _ISNB10.setText(ISBN10);
        _EAN.setText(EAN);
        _UPC.setText(UPC);
        _title.setText(title);
        _author.setText(author);
        _pages.setText(pages);
        _price.setText(price);

        _sellingPrice.setText(sellingPrice);
        _userEmail.setText(userEmail);


        if(coopPrice.equals("0.0"))
            _coopPrice.setText("Assign Price");
        else
            _coopPrice.setText(coopPrice);



        int position = ((ArrayAdapter<String>)_condition.getAdapter()).getPosition(_bookInfo.getBookCondition());

        _condition.setSelection(position);
    }

    private void onCancel() {
        if(_action.equals("Receive"))
        {
            _service.book().cancelReceive(_bookInfo);
        }
        else
            if(_action.equals("Recover"))
            {
                _service.book().cancelRecovery(_bookInfo);
            }

        Intent intent = new Intent(this, SearchBookActivity.class);
        intent.putExtra("search", _searchValue);
        intent.putExtra(EXTRA_MESSAGE, _action);
        startActivity(intent);
        finish();
    }

    private void onConfirm() {
        if(_action.equals("Receive"))
        {

              String price =   _coopPrice.getText().toString();
              String condition =  _condition.getSelectedItem().toString();


            if(!price.equals("Assign Price")&& !price.equals(_bookInfo.getCooperativePrice().toString()) )
            {
                _bookInfo.setCooperativePrice(Double.parseDouble(price));
                _bookInfo = _service.book().editCoopPrice(_bookInfo);
            }
            if(!_bookInfo.getBookCondition().equals(condition))
            {
                BookCondition c = findCondition(condition);


                _bookInfo.setBookCondition(c.getBookCondition());
                _bookInfo.setConditionId(c.getConditionId());
                _bookInfo =  _service.book().editBookCondition(_bookInfo);
            }




            _service.book().confirmReceive(_bookInfo);
        }
        else
        if(_action.equals("Recover"))
        {
            _service.book().confirmRecovery(_bookInfo);
        }

        Intent intent = new Intent(this, SearchBookActivity.class);
        intent.putExtra("search", _searchValue);
        intent.putExtra(EXTRA_MESSAGE, _action);
        startActivity(intent);
        finish();
    }
}
