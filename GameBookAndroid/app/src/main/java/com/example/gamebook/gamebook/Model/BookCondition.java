package com.example.gamebook.gamebook.Model;

import java.io.Serializable;

/**
 * Created by Julien on 11/14/2016.
 */

public class BookCondition implements Serializable {

    private int ConditionId;
    private String BookCondition;
    private double PriceFactor;

    public int getConditionId() {
        return ConditionId;
    }

    public void setConditionId(int conditionId) {
        ConditionId = conditionId;
    }

    public String getBookCondition() {
        return BookCondition;
    }

    public void setBookCondition(String bookCondition) {
        BookCondition = bookCondition;
    }

    public double getPriceFactor() {
        return PriceFactor;
    }

    public void setPriceFactor(double priceFactor) {
        PriceFactor = priceFactor;
    }
}
