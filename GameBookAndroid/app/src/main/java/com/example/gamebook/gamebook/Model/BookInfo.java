package com.example.gamebook.gamebook.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Julien on 11/12/2016.
 */

public class BookInfo implements Serializable {
    private int BookCopyId;

    private int UserId ;
    private String UserEmail;
    private String UserPhoneNumber;

    private int BookDescriptionId;
    private String ISNB10;
    private String EAN;
    private String UPC;
    private String Title;
    private String Author;
    private int NbPage;
    private Double DefaultPrice;
    private Double SellingPrice;


    private int CooperativeId;
    private int DestinationCooperativeId;
    private String CooperativeName;
    private String CooperativeAddress;
    private Double CooperativePrice;

    private int ConditionId;
    private String BookCondition;

    private int ReservationId;

    private int TransfertId;

    private String TransfertDate;

    private String ebayItemID;
    private Date EbayAddedDate;

    public int getBookCopyId() {
        return BookCopyId;
    }

    public void setBookCopyId(int bookCopyId) {
        BookCopyId = bookCopyId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public String getUserPhoneNumber() {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        UserPhoneNumber = userPhoneNumber;
    }

    public int getBookDescriptionId() {
        return BookDescriptionId;
    }

    public void setBookDescriptionId(int bookDescriptionId) {
        BookDescriptionId = bookDescriptionId;
    }

    public String getISNB10() {
        return ISNB10;
    }

    public void setISNB10(String ISNB10) {
        this.ISNB10 = ISNB10;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    public String getUPC() {
        return UPC;
    }

    public void setUPC(String UPC) {
        this.UPC = UPC;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public int getNbPage() {
        return NbPage;
    }

    public void setNbPage(int nbPage) {
        NbPage = nbPage;
    }

    public Double getDefaultPrice() {
        if(DefaultPrice == null)
            return 0.0;
        else
            return DefaultPrice;
    }

    public void setDefaultPrice(Double defaultPrice) {
        DefaultPrice = defaultPrice;
    }

    public Double getSellingPrice() {

        if(SellingPrice == null)
            return 0.0;
        else
            return SellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        SellingPrice = sellingPrice;
    }

    public int getCooperativeId() {
        return CooperativeId;
    }

    public void setCooperativeId(int cooperativeId) {
        CooperativeId = cooperativeId;
    }

    public int getDestinationCooperativeId() {
        return DestinationCooperativeId;
    }

    public void setDestinationCooperativeId(int destinationCooperativeId) {
        DestinationCooperativeId = destinationCooperativeId;
    }

    public String getCooperativeName() {
        return CooperativeName;
    }

    public void setCooperativeName(String cooperativeName) {
        CooperativeName = cooperativeName;
    }

    public String getCooperativeAddress() {
        return CooperativeAddress;
    }

    public void setCooperativeAddress(String cooperativeAddress) {
        CooperativeAddress = cooperativeAddress;
    }

    public Double getCooperativePrice() {
        if(CooperativePrice == null)
            return 0.0;
        else
             return CooperativePrice;
    }

    public void setCooperativePrice(Double cooperativePrice) {
        CooperativePrice = cooperativePrice;
    }

    public int getConditionId() {
        return ConditionId;
    }

    public void setConditionId(int conditionId) {
        ConditionId = conditionId;
    }

    public String getBookCondition() {
        return BookCondition;
    }

    public void setBookCondition(String bookCondition) {
        BookCondition = bookCondition;
    }

    public int getReservationId() {
        return ReservationId;
    }

    public void setReservationId(int reservationId) {
        ReservationId = reservationId;
    }

    public int getTransfertId() {
        return TransfertId;
    }

    public void setTransfertId(int transfertId) {
        TransfertId = transfertId;
    }

    public String getTransfertDate() {
        return TransfertDate;
    }

    public void setTransfertDate(String transfertDate) {
        TransfertDate = transfertDate;
    }

    public String getEbayItemID() {
        return ebayItemID;
    }

    public void setEbayItemID(String ebayItemID) {
        this.ebayItemID = ebayItemID;
    }

    public Date getEbayAddedDate() {
        return EbayAddedDate;
    }

    public void setEbayAddedDate(Date ebayAddedDate) {
        EbayAddedDate = ebayAddedDate;
    }
}
