package com.example.gamebook.gamebook.Activiy.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.gamebook.gamebook.Activiy.GameBookActivity;
import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;

/**
 * Created by Julien on 11/15/2016.
 */

public class UserMenu extends AppCompatActivity {


    GobService _service;
    public final static String EXTRA_MESSAGE = "searchValue";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _service = GobService.getInstance();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.logout:
                _service.user().LogOut();

                Intent nextActivityIntent = new Intent(this, GameBookActivity.class);
                startActivity(nextActivityIntent);
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}