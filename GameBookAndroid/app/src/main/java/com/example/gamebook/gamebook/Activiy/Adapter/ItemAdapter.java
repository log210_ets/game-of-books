package com.example.gamebook.gamebook.Activiy.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.gamebook.gamebook.Model.BookInfo;
import com.example.gamebook.gamebook.R;

import java.util.List;

/**
 * Created by Julien on 11/29/2016.
 */

public class ItemAdapter extends BaseAdapter {


    private List<BookInfo> _BookInfoList;
    private Context fContext;

    public ItemAdapter(Activity aActivity, List<BookInfo> BookInfoList) {

        fContext = aActivity.getApplicationContext();
        _BookInfoList = BookInfoList;
    }

    public  void  updateList( List<BookInfo> BookInfoList)
    {
        _BookInfoList = BookInfoList;
    }


    @Override
    public int getCount() {
        return _BookInfoList.size();
    }

    @Override
    public BookInfo getItem(int aPosition) {
        return _BookInfoList.get(aPosition);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) fContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_book, parent, false);

        TextView title = (TextView) row.findViewById(R.id.item_Title);
        TextView condition = (TextView) row.findViewById(R.id.item_Condition);
        TextView markerId = (TextView) row.findViewById(R.id.book_MakerID);


        BookInfo temp = _BookInfoList.get(position);

        title.setText(temp.getTitle());
        markerId.setText(Integer.toString(position));
        condition.setText(temp.getBookCondition());

        return row;
    }

}
