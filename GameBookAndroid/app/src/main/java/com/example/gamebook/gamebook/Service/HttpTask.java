package com.example.gamebook.gamebook.Service;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

/**
 * Created by Julien on 11/12/2016.
 */

public  class HttpTask {

    public ResponseEntity get(String uri,String token){

        TemplateRequest requestTemplate = new TemplateRequest(uri,
                token,
                HttpMethod.GET,
                null
                );

        ResponseEntity reponse = null;
        try {

            reponse = new AsynHttpTask().execute(requestTemplate).get(5000, TimeUnit.MILLISECONDS);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }


        return reponse;

    }

    public ResponseEntity post(String uri,String token,Object request){

         TemplateRequest requestTemplate = new TemplateRequest(uri,
                token,
                HttpMethod.POST,
                request);

        ResponseEntity reponse = null;
        try {

             reponse = new AsynHttpTask().execute(requestTemplate).get(5000, TimeUnit.MILLISECONDS);

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
;
            }


        return reponse;
    }


    public class AsynHttpTask extends AsyncTask<TemplateRequest, Void, ResponseEntity> {
        @Override
        protected ResponseEntity doInBackground(TemplateRequest... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                TemplateRequest request = params[0];

                HttpMethod httpMethod = request.getHttpMethod();



                // set headers
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);



                headers.set("Authorization",request.getToken());

                Object sendObject = request.getRequest();

                // Add the send object + headers
                HttpEntity<Object> entity = new HttpEntity<>(sendObject, headers);


                // Add the Jackson message converter
                restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());

              //  restTemplate.postForObject( request.getUri(), entity,request.getResponseType().getClass());

                ResponseEntity responseEntity =restTemplate.exchange( request.getUri(),
                         httpMethod,
                         entity, Object.class );


                return responseEntity;

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return  null;
        }
    }
}
