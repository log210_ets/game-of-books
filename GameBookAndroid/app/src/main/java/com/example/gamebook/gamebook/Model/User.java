package com.example.gamebook.gamebook.Model;

/**
 * Created by Julien on 10/14/2016.
 */

public class User {

    private String Username;
    private String Password;
    private String Email;
    private Boolean IsAuthentified;
    private String SessionToken;
    private String[] Roles;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        this.Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public Boolean getIsAuthentified() {
        return IsAuthentified;
    }

    public void setIsAuthentified(Boolean isAuthentified) {
        IsAuthentified = isAuthentified;
    }

    public String getSessionToken() {
        return SessionToken;
    }

    public void setSessionToken(String sessionToken) {
        SessionToken = sessionToken;
    }

    public String[] getRoles() {
        return Roles;
    }

    public void setRoles(String[] roles) {
        Roles = roles;
    }
}
