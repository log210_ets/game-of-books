package com.example.gamebook.gamebook.Service.Services;

import android.util.Log;

import com.example.gamebook.gamebook.Model.BookCopy;
import com.example.gamebook.gamebook.Model.BookInfo;
import com.example.gamebook.gamebook.Model.SearchObject;
import com.google.gson.Gson;

import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Julien on 11/12/2016.
 */

public class BookService extends Service {

    public BookService() {
        super("book/");
    }


    public BookInfo searchBook(String search) {
        String uri = "SearchBook";

        SearchObject object = new SearchObject();
        object.setValue(search);

        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), object);

        BookInfo bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);


        return bookInfo;
    }

    public BookInfo Save(BookCopy bookCopy) {
        String uri = "Save";

        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookCopy);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }


        return bookInfo;
    }

    public List<BookInfo> searchBookToRecover(String search) {
        String uri = "searchBookToRecover";

        SearchObject object = new SearchObject();
        object.setValue(search);

        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), object);

        BookInfo[] bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo[].class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }


        return new ArrayList<BookInfo>(Arrays.asList(bookInfo));
    }

    public List<BookInfo> searchBookToReceive(String search) {
        String uri = "searchBookToReceive";

        SearchObject object = new SearchObject();
        object.setValue(search);

        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), object);

        BookInfo[] bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo[].class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return new ArrayList<BookInfo>(Arrays.asList(bookInfo));
    }

    public BookInfo cancelReceive(BookInfo bookInfos) {
        String uri = "cancelReceive";


        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookInfos);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return bookInfo;
    }

    public BookInfo editCoopPrice(BookInfo bookInfos) {
        String uri = "editCoopPrice";


        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookInfos);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return bookInfo;
    }

    public BookInfo editBookCondition(BookInfo bookInfos) {
        String uri = "editBookCondition";


        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookInfos);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return bookInfo;
    }

    public BookInfo confirmRecovery(BookInfo bookInfos) {
        String uri = "confirmRecovery";


        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookInfos);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return bookInfo;
    }

    public BookInfo confirmReceive(BookInfo bookInfos) {
        String uri = "confirmReceive";


        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookInfos);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return bookInfo;
    }
    public BookInfo cancelRecovery(BookInfo bookInfos) {
        String uri = "cancelRecovery";


        ResponseEntity responseEntity = _http.post(GetUri(uri), GetToken(), bookInfos);

        BookInfo bookInfo = null;
        try {
            bookInfo = new Gson().fromJson(responseEntity.getBody().toString(), BookInfo.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return bookInfo;
    }

}
