package com.example.gamebook.gamebook.Activiy.User;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.example.gamebook.gamebook.Model.BookCondition;
import com.example.gamebook.gamebook.Model.BookCopy;
import com.example.gamebook.gamebook.Model.BookInfo;
import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class AddBookActivity extends UserMenu {


    TextView _searchResultInfo;
    EditText _ISNB10, _EAN, _UPC, _title, _author, _pages, _price;
    MaterialBetterSpinner _condition;
    private FloatingSearchView mSearchView;
    BookCondition[] _conditions;
    private BookInfo _bookInfo;
    Button _sendButton;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        _service = GobService.getInstance();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        return true;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);

        ScrollView view = (ScrollView) findViewById(R.id.scrollViewAddBook);
        view.setVisibility(View.INVISIBLE);

        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupFloatingSearch();
        initializeComponent();

        String querry = getIntent().getStringExtra(EXTRA_MESSAGE);

        if (querry != null) {
            mSearchView.setSearchText(querry);
            searchBook(querry);
        }

        _sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addBook();
            }

        });
    }


    private void initializeComponent() {

          _conditions = _service.bookCondition().getBookCondition();

        String[] _bookConditions = new String[_conditions.length];

        for (int i = 0; i < _conditions.length; i++) {
            _bookConditions[i] = _conditions[i].getBookCondition();
        }


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, _bookConditions);
        _condition = (MaterialBetterSpinner)
                findViewById(R.id.input_Condition);
        _condition.setAdapter(arrayAdapter);


        _searchResultInfo = (TextView) findViewById(R.id.searchResultInfo);
        _ISNB10 = (EditText) findViewById(R.id.input_ISNB10);
        _EAN = (EditText) findViewById(R.id.input_EAN);
        _UPC = (EditText) findViewById(R.id.input_UPC);
        _title = (EditText) findViewById(R.id.input_Title);
        _author = (EditText) findViewById(R.id.input_Author);
        _pages = (EditText) findViewById(R.id.input_pages);
        _price = (EditText) findViewById(R.id.input_Price);
        _sendButton = (Button) findViewById(R.id.btn_confirm);

    }

    private void setupFloatingSearch() {
        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {


            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String query) {

                if (query.trim().equals(""))
                    return;


                searchBook(query);
            }
        });
    }

    private void searchBook(String query) {
        _bookInfo = _service.book().searchBook(query);

        ScrollView view = (ScrollView) findViewById(R.id.scrollViewAddBook);
        view.setVisibility(View.VISIBLE);

        populateForm();
    }

    private void populateForm() {


        if (_bookInfo == null) {
            _searchResultInfo.setText(R.string.no_book_matches_with_searching_criteria);
            _searchResultInfo.setTextColor(Color.parseColor("#ff0000"));

            populateField("", "", "", "", "", "", "");
        } else {
            _searchResultInfo.setText(R.string.a_book_matches_with_searching_criteria);
            _searchResultInfo.setTextColor(Color.parseColor("#33cc33"));

            populateField(_bookInfo.getISNB10(),
                    _bookInfo.getEAN(),
                    _bookInfo.getUPC(),
                    _bookInfo.getTitle(),
                    _bookInfo.getAuthor(),
                    String.valueOf(_bookInfo.getNbPage()),
                    _bookInfo.getDefaultPrice().toString());
        }


    }

    private void populateField(String ISBN10, String EAN, String UPC, String title, String author, String pages, String price) {

        _ISNB10.setText(ISBN10);
        _EAN.setText(EAN);
        _UPC.setText(UPC);
        _title.setText(title);
        _author.setText(author);
        _pages.setText(pages);
        _price.setText(price);
    }


    private void addBook() {

        if (!validate()) {
            Toast bread = Toast.makeText(getApplicationContext(),"Missing somes informations.", Toast.LENGTH_LONG);
            bread.show();
            return;
        }
        setBooKInfo();

        //TODO les autres
        BookCopy bookCopy = new BookCopy();
        bookCopy.setBookDescription(_bookInfo);
        BookCondition condition = findCondition(_condition.getText().toString());
        bookCopy.setCondition(condition);

      BookInfo book =  _service.book().Save(bookCopy);

        if(book != null){
            Toast bread = Toast.makeText(getApplicationContext(),"Success!!, Your book is now added.", Toast.LENGTH_LONG);
            bread.show();
            callUserMenu();
        }else
        {
            Toast bread = Toast.makeText(getApplicationContext(),"Error while adding the book", Toast.LENGTH_LONG);
            bread.show();
        }


    }

    private void setBooKInfo() {
        _bookInfo = new BookInfo();

        String ISNB10 = _ISNB10.getText().toString();
        String author =_author.getText().toString();
        String EAN = _EAN.getText().toString();
        String UPC = _UPC.getText().toString();
        String title = _title.getText().toString();
        String page = _pages.getText().toString();
        String price = _price.getText().toString();

        int nbPage;
        Double prices;
        try{
             nbPage = Integer.parseInt(page);
             prices = Double.parseDouble(price);


            _bookInfo.setTitle(title);
            _bookInfo.setAuthor(author);

            _bookInfo.setISNB10(ISNB10);
            _bookInfo.setEAN(EAN);
            _bookInfo.setUPC(UPC);
            _bookInfo.setDefaultPrice(prices);
            _bookInfo.setNbPage(nbPage);

        }
        catch (Exception  ex)
        {

        }


    }

    private void callUserMenu() {
        Intent intent = new Intent(this, UserMainActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean validate() {
        boolean valid = true;



        String title = _title.getText().toString();
        String author = _author.getText().toString();
        String condition = _condition.getText().toString();
        String price = _price.getText().toString();



        _ISNB10.setError(null);
        _EAN.setError(null);
        _UPC.setError(null);
        _pages.setError(null);
        _price.setError(null);

        if (title.isEmpty() ) {
            _title.setError("Enter Valid title");
            valid = false;
        } else {
            _title.setError(null);
        }
        if (author.isEmpty() ) {
            _author.setError("Enter Valid author");
            valid = false;
        } else {
            _author.setError(null);
        }
        if (condition.isEmpty() ) {
            _condition.setError("Enter Valid condition");
            valid = false;
        } else {
            _condition.setError(null);
        }



        return valid;

    }

    private  BookCondition  findCondition(String conditon){
        for (int i = 0; i < _conditions.length ; i++)
        {
            if(_conditions[i].getBookCondition().equals(conditon))
                return _conditions[i];

        }
        return  null;
    }

}
