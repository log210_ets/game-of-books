package com.example.gamebook.gamebook.Service.Services;

import com.example.gamebook.gamebook.Model.User;
import com.example.gamebook.gamebook.Service.HttpTask;

/**
 * Created by Julien on 11/12/2016.
 */

    abstract class Service {
    GobService _service;
    protected String token;
    private final  String BASE_URI = "http://192.168.1.147:8081/api/";
    //private final  String BASE_URI = "https://api.3am-eternal.org/api/";

    private final  String _context;
    protected HttpTask _http = new HttpTask();


    Service(String context){
        _context = context;


    }

    String GetUri(String url)
    {
        return  BASE_URI + _context + url;
    }

    String GetToken(){
           _service = GobService.getInstance();

        User user = _service.getLogUser();

        if(user!= null)
            return  user.getSessionToken();

        // Not connected
        return "empty";
    }
}
