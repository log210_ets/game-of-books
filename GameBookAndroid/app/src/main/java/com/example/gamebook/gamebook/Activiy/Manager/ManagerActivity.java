package com.example.gamebook.gamebook.Activiy.Manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.example.gamebook.gamebook.Activiy.User.UserMenu;
import com.example.gamebook.gamebook.R;
import com.example.gamebook.gamebook.Service.Services.GobService;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

public class ManagerActivity   extends UserMenu {
    GobService _service;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionBarCode, floatingActionButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        _service = GobService.getInstance();

        final Activity activity = this;

        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionBarCode = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item2);

        floatingActionBarCode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                receiveBook();
            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                recoverBook();
            }
        });
    }

    private void recoverBook() {
        openSearchBookActivity("Recover");
    }

    private void receiveBook() {
        openSearchBookActivity("Receive");
    }

    private void openSearchBookActivity(String activity) {
        Intent intent = new Intent(this, SearchBookActivity.class);

        intent.putExtra(EXTRA_MESSAGE, activity);

        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;


    }
}
