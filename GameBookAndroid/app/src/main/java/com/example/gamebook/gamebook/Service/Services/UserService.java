package com.example.gamebook.gamebook.Service.Services;

import com.example.gamebook.gamebook.Model.User;
import com.google.gson.Gson;

import org.springframework.http.ResponseEntity;

/**
 * Created by Julien on 11/12/2016.
 */

  public class UserService  extends Service {

    public UserService()
    {
        super("user/");
    }

    public User Login(User user)  {
        String uri = "login";

         ResponseEntity responseEntity = _http.post(GetUri(uri),GetToken(),user);

            user = new Gson().fromJson(responseEntity.getBody().toString(), User.class);
        // conversion

        return   user;
    }

    public User GetLoggedUser(String token){
        String uri = "GetLoggedUser";

        ResponseEntity responseEntity = _http.get(GetUri(uri),token);

        User user = new Gson().fromJson(responseEntity.getBody().toString(), User.class);

        return  user;
    }

    public Boolean LogOut(){
        String uri = "logout";

        ResponseEntity responseEntity = _http.post(GetUri(uri),GetToken(),"");

        Boolean successful = new Gson().fromJson(responseEntity.getBody().toString(), Boolean.class);

        return  successful;
    }
}
