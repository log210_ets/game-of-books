--
--------------------
--DROP UNUSED TABLES
--------------------
IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Manager') AND Type = N'U')
BEGIN
	DROP TABLE Manager
END

GO

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Student') AND Type = N'U')
BEGIN
	DROP TABLE Student
END
GO

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.CopyReservation') AND Type = N'U')
BEGIN
	DROP TABLE CopyReservation
END
GO

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.TransferBookCopies') AND Type = N'U')
BEGIN
	DROP TABLE TransferBookCopies
END
GO

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U')
BEGIN
	DROP TABLE Reservations
END

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Transfers') AND Type = N'U')
BEGIN
	DROP TABLE Transfers
END
GO


GO
--------------------------------------------------------------------------------
--CREATE TABLES
--------------------------------------------------------------------------------

-- TABLE Users
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U')
BEGIN
	CREATE TABLE Users
	(
		UserId INT NOT NULL IDENTITY,
		PhoneNumber NVARCHAR(250) NULL,
		Email NVARCHAR(250) NULL,
		RegistrationDate DATE NOT NULL,
		CreditCardNumber NVARCHAR(MAX) NULL,
		Password NVARCHAR(MAX) NOT NULL,
		SessionToken NVARCHAR(MAX) NULL,

		CooperativeId INT NULL, -- FK

		CONSTRAINT PK_Users PRIMARY KEY(UserId)
	)
END
 


-- TABLE Cooperatives
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Cooperatives') AND Type = N'U')
BEGIN
	CREATE TABLE Cooperatives
	(
		CooperativeId INT NOT NULL IDENTITY,
		Name NVARCHAR(250) NULL,
		Address NVARCHAR(MAX) NULL,

		CONSTRAINT PK_CooperativeId PRIMARY KEY(CooperativeId)
	)
END
 


-- TABLE Roles
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Roles') AND Type = N'U')
BEGIN
	CREATE TABLE Roles
	(
		RoleId INT NOT NULL IDENTITY,
		Name NVARCHAR(250) NULL UNIQUE,

		CONSTRAINT PK_Roles PRIMARY KEY(RoleId)
	)
END


-- TABLE UserRoles
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.UserRoles') AND Type = N'U')
BEGIN
	CREATE TABLE UserRoles
	(
		UserId INT NOT NULL,
		RoleId INT NOT NULL,
		CONSTRAINT PK_UserRoles PRIMARY KEY(UserId, RoleId)
	)
END



-- TABLE Books
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookDescriptions') AND Type = N'U')
BEGIN
	CREATE TABLE BookDescriptions
	(
		BookDescriptionId INT NOT NULL IDENTITY,
		ISNB10 NVARCHAR(50) NULL,
		EAN  NVARCHAR(50)   NULL,
		UPC NVARCHAR(50)    NULL,
		Title NVARCHAR(250) NULL,
		Author NVARCHAR(250)NULL,
		NbPage INT NULL,
		DefaultPrice MONEY NULL,

		CooperativeId INT NULL, -- FK

		CONSTRAINT PK_BookDescriptions PRIMARY KEY(BookDescriptionId)
	)
END



-- TABLE BookCopies
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U')
BEGIN
	CREATE TABLE BookCopies
	(
		BookCopyId INT NOT NULL IDENTITY,
		BookDescriptionId INT NULL,
		SellerId INT NOT NULL, -- Student from Users,
		ConditionId INT NOT NULL,
		CooperativeId INT NOT NULL,
		IsReceived BIT NULL,
		ReceivedDate DATE NULL,

		CONSTRAINT PK_BookCopies PRIMARY KEY(BookCopyId)
	)
END
IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND
 NOT EXISTS(SELECT * FROM sys.columns WHERE Name= N'Canceled' AND Object_ID = Object_ID(N'BookCopies'))
BEGIN
    ALTER TABLE BookCopies ADD Canceled BIT NULL
END


--TABLE Reservations
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U')
BEGIN
	CREATE TABLE Reservations
	(
		ReservationId INT NOT NULL IDENTITY,
		TransferId INT NULL,
		BookCopyId INT NOT NULL,
		FromCooperativeId INT NOT NULL,
		ToCooperativeId INT NULL,
		UserId INT NOT NULL,
		
		ExpirationDate DATE NULL,
		ReceiptDate DATE NULL,

		CONSTRAINT PK_Reservations PRIMARY KEY(ReservationId)
	)
END

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND
 NOT EXISTS(SELECT * FROM sys.columns WHERE Name= N'Canceled' AND Object_ID = Object_ID(N'BookCopies'))
BEGIN
    ALTER TABLE BookCopies ADD Canceled BIT NULL
END
-- TABLE Conditions
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Conditions') AND Type = N'U')
BEGIN
	CREATE TABLE Conditions
	(
		ConditionId INT NOT NULL IDENTITY,
		BookCondition NVARCHAR(MAX) NULL,

		CONSTRAINT PK_Conditions PRIMARY KEY(ConditionId)
	)
END

GO

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Conditions') AND Type = N'U') AND
 NOT EXISTS(SELECT * FROM sys.columns WHERE Name= N'BookConditionFr' AND Object_ID = Object_ID(N'Conditions'))
BEGIN
    ALTER TABLE Conditions ADD BookConditionFr NVARCHAR(MAX) NULL
END
GO

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Conditions') AND Type = N'U') AND
 NOT EXISTS(SELECT * FROM sys.columns WHERE Name= N'PriceFactor' AND Object_ID = Object_ID(N'Conditions'))
BEGIN
    ALTER TABLE Conditions ADD PriceFactor FLOAT NULL
END

GO
-- TABLE BookPrices
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookPrices') AND Type = N'U')
BEGIN
	CREATE TABLE BookPrices
	(
		CooperativeId INT NOT NULL,
		BookDescriptionId INT NOT NULL,
		CoopPrice MONEY NULL,

		CONSTRAINT PK_BookPrices PRIMARY KEY(CooperativeId, BookDescriptionId)
	)
END

-- TABLE StringResources
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.StringResources') AND Type = N'U')
BEGIN
	CREATE TABLE StringResources
	(
		StringResourceId INT NOT NULL IDENTITY,
		StringKey NVARCHAR(MAX) NOT NULL,
		StringEn NVARCHAR(MAX) NOT NULL,
		StringFr NVARCHAR(MAX) NOT NULL,

		CONSTRAINT PK_StringResources PRIMARY KEY(StringResourceId)
	)
END

-- TABLE Ebay
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Ebay') AND Type = N'U')
BEGIN
	CREATE TABLE Ebay
	(
		BookCopyId INT UNIQUE NOT NULL,	
		EbayItemID NVARCHAR(MAX) NOT NULL,
		AddedDate DATETIME NOT NULL,

		CONSTRAINT PK_Ebay PRIMARY KEY(BookCopyId)
	)
END
ALTER TABLE dbo.Ebay ALTER COLUMN AddedDate datetime not null

-- TABLE Braintree
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Braintree') AND Type = N'U')
BEGIN
	CREATE TABLE Braintree
	(
		ReservationId INT UNIQUE NOT NULL,	
		BraintreeOrderID NVARCHAR(MAX) NOT NULL,

		CONSTRAINT PK_Braintree PRIMARY KEY(ReservationId)
	)
END

-- TABLE Transfers
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Transfers') AND Type = N'U')
BEGIN
	CREATE TABLE Transfers
	(
		TransferId INT NOT NULL IDENTITY,
		ExpeditionDate DATETIME NOT NULL,
		ReceiptDate DATETIME NULL

		CONSTRAINT PK_Transfers PRIMARY KEY(TransferId)
	)
END

-- TABLE ReservationDemands
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.ReservationDemands') AND Type = N'U')
BEGIN
	CREATE TABLE ReservationDemands
	(
		ReservationDemandId INT NOT NULL IDENTITY,
		UserId INT NOT NULL,
		NotificationDate DATETIME NULL,
		ISNB10 NVARCHAR(50) NULL,

		CONSTRAINT PK_ReservationDemands PRIMARY KEY(ReservationDemandId)
	)
END

--------------------------------------------------------------------------------
--CREATE FOREIGN KEYS
--------------------------------------------------------------------------------

-- FK_UserRoles_Users
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.UserRoles') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_UserRoles_Users') AND parent_object_id = OBJECT_ID(N'dbo.UserRoles')) 
BEGIN
	ALTER TABLE UserRoles ADD CONSTRAINT FK_UserRoles_Users  FOREIGN KEY (UserId) REFERENCES Users(UserId)
END



-- FK_UserRoles_Roles
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.UserRoles') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Roles') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_UserRoles_Roles') AND parent_object_id = OBJECT_ID(N'dbo.UserRoles')) 
BEGIN
	ALTER TABLE UserRoles ADD CONSTRAINT FK_UserRoles_Roles  FOREIGN KEY (RoleId) REFERENCES Roles(RoleId)
END


-- FK_Users_Cooperatives
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Cooperatives') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Users_Cooperatives') AND parent_object_id = OBJECT_ID(N'dbo.Users')) 
BEGIN
	ALTER TABLE Users ADD CONSTRAINT FK_Users_Cooperatives  FOREIGN KEY (CooperativeId) REFERENCES Cooperatives(CooperativeId)
END



-- FK_BookCopies_Users
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookCopies_Users') AND parent_object_id = OBJECT_ID(N'dbo.BookCopies')) 
BEGIN
	ALTER TABLE BookCopies ADD CONSTRAINT FK_BookCopies_Users  FOREIGN KEY (SellerId) REFERENCES Users(UserId)
END


-- FK_BookCopies_Cooperatives
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Cooperatives') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookCopies_Cooperatives') AND parent_object_id = OBJECT_ID(N'dbo.BookCopies')) 
BEGIN
	ALTER TABLE BookCopies ADD CONSTRAINT FK_BookCopies_Cooperatives  FOREIGN KEY (CooperativeId) REFERENCES Cooperatives(CooperativeId)
END

-- FK_CopyReservation_BookCopyId
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.CopyReservation') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookCopies_CopyReservation') AND parent_object_id = OBJECT_ID(N'dbo.CopyReservation')) 
BEGIN
	ALTER TABLE CopyReservation ADD CONSTRAINT FK_BookCopies_CopyReservation  FOREIGN KEY (BookCopyId) REFERENCES BookCopies(BookCopyId)
END 

-- FK_CopyReservation_UserId
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.CopyReservation') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Users_CopyReservation') AND parent_object_id = OBJECT_ID(N'dbo.CopyReservation')) 
BEGIN
	ALTER TABLE CopyReservation ADD CONSTRAINT FK_Users_CopyReservation  FOREIGN KEY (UserId) REFERENCES Users(UserId)
END


-- FK_BookCopies_Conditions
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Conditions') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookCopies_Conditions') AND parent_object_id = OBJECT_ID(N'dbo.BookCopies')) 
BEGIN
	ALTER TABLE BookCopies ADD CONSTRAINT FK_BookCopies_Conditions  FOREIGN KEY (ConditionId) REFERENCES Conditions(ConditionId)
END

-- FK_BookCopies_BookDescriptions
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookDescriptions') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookCopies_BookDescriptions') AND parent_object_id = OBJECT_ID(N'dbo.BookCopies')) 
BEGIN
	ALTER TABLE BookCopies ADD CONSTRAINT FK_BookCopies_BookDescriptions  FOREIGN KEY (BookDescriptionId) REFERENCES BookDescriptions(BookDescriptionId)
END


-- FK_BookPrices_BookDescriptions
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookPrices') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookDescriptions') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookPrices_BookDescriptions') AND parent_object_id = OBJECT_ID(N'dbo.BookPrices')) 
BEGIN
	ALTER TABLE BookPrices ADD CONSTRAINT FK_BookPrices_BookDescriptions  FOREIGN KEY (BookDescriptionId) REFERENCES BookDescriptions(BookDescriptionId)
END


-- FK_BookPrice_Cooperatives
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookPrices') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Cooperatives') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookPrice_Cooperatives') AND parent_object_id = OBJECT_ID(N'dbo.BookPrices')) 
BEGIN
	ALTER TABLE BookPrices ADD CONSTRAINT FK_BookPrice_Cooperatives  FOREIGN KEY (CooperativeId) REFERENCES Cooperatives(CooperativeId)
END

-- FK_BookCopies_Ebay
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Ebay') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_BookCopies_Ebay') AND parent_object_id = OBJECT_ID(N'dbo.Ebay')) 
BEGIN
	ALTER TABLE Ebay ADD CONSTRAINT FK_BookCopies_Ebay  FOREIGN KEY (BookCopyId) REFERENCES BookCopies(BookCopyId)
END

-- FK_Reservations_Braintree
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Braintree') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Reservations_Braintree') AND parent_object_id = OBJECT_ID(N'dbo.Braintree')) 
BEGIN
	ALTER TABLE Braintree ADD CONSTRAINT FK_Reservations_Braintree  FOREIGN KEY (ReservationId) REFERENCES Reservations(ReservationId)
END

----------------------------------------------------
-- FK_Reservations_BookCopies
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.BookCopies') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Reservations_BookCopies') AND parent_object_id = OBJECT_ID(N'dbo.Reservations')) 
BEGIN
	ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_BookCopies  FOREIGN KEY (BookCopyId) REFERENCES BookCopies(BookCopyId)
END


-- FK_Reservations_Users
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Reservations_Users') AND parent_object_id = OBJECT_ID(N'dbo.Reservations')) 
BEGIN
	ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_Users  FOREIGN KEY (UserId) REFERENCES Users(UserId)
END


-- FK_Reservations_FromCooperatives
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Cooperatives') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Reservations_FromCooperatives') AND parent_object_id = OBJECT_ID(N'dbo.Reservations')) 
BEGIN
	ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_FromCooperatives  FOREIGN KEY (FromCooperativeId) REFERENCES Cooperatives(CooperativeId)


-- FK_Reservations_ToCooperatives
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Cooperatives') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Reservations_ToCooperatives') AND parent_object_id = OBJECT_ID(N'dbo.Reservations')) 
BEGIN
	ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_ToCooperatives  FOREIGN KEY (ToCooperativeId) REFERENCES Cooperatives(CooperativeId)
END

-- FK_Reservations_Transfers
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Reservations') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Transfers') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_Reservation_Transfers') AND parent_object_id = OBJECT_ID(N'dbo.Reservations')) 
BEGIN
	ALTER TABLE Reservations ADD CONSTRAINT FK_Reservations_Transfers  FOREIGN KEY (TransferId) REFERENCES Transfers(TransferId)
END

-- FK_ReservationDemands_Users
IF	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.ReservationDemands') AND Type = N'U') AND 
	EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Users') AND Type = N'U') AND 
    NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_ReservationDemands_Users') AND parent_object_id = OBJECT_ID(N'dbo.ReservationDemands')) 
BEGIN
	ALTER TABLE ReservationDemands ADD CONSTRAINT FK_ReservationDemands_Users  FOREIGN KEY (UserId) REFERENCES Users(UserId)
END

END

--------------------
--INSERT DATA
--------------------
DECLARE @RoleStudent NVARCHAR(150) = 'Student'
DECLARE @RoleManager NVARCHAR(150) = 'Manager'
DECLARE @RoleDev NVARCHAR(150) = 'Dev'

IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Roles WHERE Name=@RoleStudent)BEGIN INSERT INTO dbo.Roles ( Name) VALUES (@RoleStudent) END
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Roles WHERE Name=@RoleManager)BEGIN INSERT INTO dbo.Roles ( Name) VALUES (@RoleManager) END
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Roles WHERE Name=@RoleDev)BEGIN INSERT INTO dbo.Roles ( Name) VALUES (@RoleDev) END

IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='New')BEGIN INSERT INTO dbo.Conditions (BookCondition) VALUES ('New') END
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='Used')BEGIN INSERT INTO dbo.Conditions (BookCondition) VALUES ('Used') END
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='Damaged')BEGIN INSERT INTO dbo.Conditions (BookCondition) VALUES ('Damaged') END

IF EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='New')UPDATE dbo.Conditions SET PriceFactor = 0.75 WHERE BookCondition='New'
IF EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='Used')UPDATE dbo.Conditions SET PriceFactor = 0.5 WHERE BookCondition='Used'
IF EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='Damaged')UPDATE dbo.Conditions SET PriceFactor = 0.25 WHERE BookCondition='Damaged'

IF EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='New')UPDATE dbo.Conditions SET BookConditionFr = 'Neuf' WHERE BookCondition='New'
IF EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='Used')UPDATE dbo.Conditions SET BookConditionFr = 'Usagé' WHERE BookCondition='Used'
IF EXISTS (SELECT TOP 1 * FROM dbo.Conditions WHERE BookCondition='Damaged')UPDATE dbo.Conditions SET BookConditionFr = 'Endommagé' WHERE BookCondition='Damaged'

IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Cooperatives WHERE Name='Unassigned')BEGIN INSERT INTO dbo.Cooperatives (Name,Address) VALUES ('Unassigned','Do not delete') END
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Cooperatives WHERE Name='Coop Éts')BEGIN INSERT INTO dbo.Cooperatives (Name,Address) VALUES ('Coop Éts','1100 Rue Notre-Dame O, Montréal, QC H3C 1K3') END
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Cooperatives WHERE Name='Coop Mcgill')BEGIN INSERT INTO dbo.Cooperatives (Name,Address) VALUES ('Coop Mcgill','845 Rue Sherbrooke O, Montréal, QC H3A 0G4') END

-- insert dev user
IF NOT EXISTS(SELECT TOP 1 * FROM dbo.Users WHERE Email='d@d')BEGIN INSERT INTO dbo.Users (Email, Password, RegistrationDate) VALUES ('d@d','d', GETDATE()) END
IF EXISTS (SELECT TOP 1 * FROM dbo.Cooperatives WHERE Name='Coop Éts') AND EXISTS(SELECT TOP 1 * FROM dbo.Users WHERE Email='d@d')BEGIN UPDATE Users SET CooperativeId = (SELECT TOP 1 CooperativeId FROM Cooperatives WHERE Name='Coop Éts') WHERE Email='d@d'END

DECLARE @DevUserId INT = (SELECT TOP 1 UserId FROM Users WHERE Email='d@d')
DECLARE @DevRoleId INT = (SELECT TOP 1 RoleId FROM Roles WHERE Name=@RoleDev)

IF NOT EXISTS(SELECT TOP 1 * FROM dbo.UserRoles WHERE UserId=@DevUserId AND RoleId=@DevRoleId)BEGIN INSERT INTO dbo.UserRoles(RoleId, UserId) VALUES (@DevRoleId, @DevUserId) END