﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.BusinessLayer;
using API.Models;
using API.Tests.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace API.Tests
{
    [TestClass]
    public class BookBusinessTest
    {
        private readonly GobEntities _dbTest = new GobEntitiesTest();
        private readonly string _sessionToken = "sjdf2fd1f";
        private User _defaultUser;

        private void ClearDatabase()
        {

            _dbTest.Database.ExecuteSqlCommand("delete from [BookCopies]");
            _dbTest.Database.ExecuteSqlCommand("delete from [UserRoles]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Users]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Roles]");
            _dbTest.Database.ExecuteSqlCommand("delete from [BookPrices]");
            _dbTest.Database.ExecuteSqlCommand("delete from [BookDescriptions]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Conditions]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Cooperatives]");
            _dbTest.Database.ExecuteSqlCommand("delete from [StringResources]");
            _dbTest.SaveChanges();
        }

        [TestInitialize]
        public void InitializeTest()
        {
            ClearDatabase();

            List<Role> roles = new List<Role>
            {
                new Role {Name = "Manager"}
            };

            var coop = new Cooperative {Name = "Unassigned"};

            _defaultUser = new User
            {
                Roles = new List<Role> { roles[0] },
                Email = "juli3211111dden128@hotmadil.com",
                Password = "123456",
                SessionToken = _sessionToken,
                Cooperative = coop
            };

            _dbTest.Conditions.Add(new Condition {BookCondition = "Used"});
            _dbTest.Cooperatives.Add(coop);
            _dbTest.Roles.AddRange(roles);
            _dbTest.Users.Add(_defaultUser);
            _dbTest.SaveChanges();
        }

        [TestCleanup]
        public void CleanUp()
        {
            ClearDatabase();
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Book is null")]
        public void TestSave_ShouldThrow_WithMissingBook()
        {
            new BookBusiness(_dbTest, _sessionToken).Save(null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Book condition is null")]
        public void TestSave_ShouldThrow_WithMissingCondition()
        {
            BookCopy bookCopy = new BookCopy
            {
                Condition = null,
                BookDescription = new BookDescription
                {
                    Author = "Frank O'Connor",
                    Title = "Halo: Tales from Slipspace",
                    ISNB10 = "1506700721",
                    EAN = "9781506700724",
                    NbPage = 96,
                    DefaultPrice = 2599
                },
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Book description is null")]
        public void TestSave_ShouldThrow_WithMissingDescription()
        {
            BookCopy bookCopy = new BookCopy
            {
                Condition = new Condition
                {
                    BookCondition = "Used"
                },
                BookDescription = null,
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "error in default coop assigment, RUN SCRIPT SQL!!!")]
        public void TestSave_ShouldThrow_IfUnassignedCooperativeNotExists()
        {
            foreach (var user in _dbTest.Users)
            {
                user.Cooperative = null;
            }

            _dbTest.SaveChanges();
            _dbTest.Database.ExecuteSqlCommand("delete from [Cooperatives]");
            _dbTest.SaveChanges();

            BookCopy bookCopy = new BookCopy
            {
                Condition = new Condition
                {
                    BookCondition = "Used"
                },
                BookDescription = null,
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Book condition isn't valid")]
        public void TestSave_ShouldThrow_IfConditionNotExists()
        {
            BookCopy bookCopy = new BookCopy
            {
                Condition = new Condition
                {
                    BookCondition = "Unvalid!!!"
                },
                BookDescription = new BookDescription
                {
                    Author = "Frank O'Connor",
                    Title = "Halo: Tales from Slipspace",
                    ISNB10 = "1506700721",
                    EAN = "9781506700724",
                    NbPage = 96,
                    DefaultPrice = 2599
                },
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);
        }

        [TestMethod]
        public void TestSave_ShouldAddDescription_IfNotExists()
        {
            var condition = _dbTest.Conditions.FirstOrDefault();

            BookCopy bookCopy = new BookCopy
            {
                Condition = condition,
                BookDescription = new BookDescription
                {
                    Author = "Frank O'Connor",
                    Title = "Halo: Tales from Slipspace",
                    ISNB10 = "1506700721",
                    EAN = "9781506700724",
                    NbPage = 96,
                    DefaultPrice = 2599
                },
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            Assert.IsTrue(!_dbTest.BookDescriptions.Any());

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);

            Assert.IsTrue(_dbTest.BookDescriptions.Count() == 1);
            Assert.IsTrue(_dbTest.BookDescriptions.FirstOrDefault()?.Title == "Halo: Tales from Slipspace");

        }

        [TestMethod]
        public void TestSave_ShouldAssignDescription_IfExists()
        {
            BookDescription bookDescription = new BookDescription
            {
                Author = "Frank O'Connor",
                Title = "Halo: Tales from Slipspace",
                ISNB10 = "1506700721",
                EAN = "9781506700724",
                NbPage = 96,
                DefaultPrice = 2599
            };

            Assert.IsTrue(!_dbTest.BookDescriptions.Any());

            _dbTest.BookDescriptions.Add(bookDescription);
            _dbTest.SaveChanges();

            Assert.IsTrue(_dbTest.BookDescriptions.Count() == 1);

            BookCopy bookCopy = new BookCopy
            {
                Condition = _dbTest.Conditions.FirstOrDefault(),
                BookDescription = bookDescription,
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);
            Assert.IsTrue(_dbTest.BookDescriptions.Count() == 1);
        }

        [TestMethod]
        public void TestSave_UserShouldBeAssociatedToBook()
        {
            var condition = _dbTest.Conditions.FirstOrDefault();

            BookCopy bookCopy = new BookCopy
            {
                Condition = condition,
                BookDescription = new BookDescription
                {
                    Author = "Frank O'Connor",
                    Title = "Halo: Tales from Slipspace",
                    ISNB10 = "1506700721",
                    EAN = "9781506700724",
                    NbPage = 96,
                    DefaultPrice = 2599
                },
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            Assert.IsTrue(!_dbTest.BookDescriptions.Any());

            new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);

            Assert.IsTrue(_dbTest.Users.Include("BookCopies").FirstOrDefault()?.BookCopies.Count() == 1);
            Assert.IsTrue(
                _dbTest.Users.Include("BookCopies").FirstOrDefault()?.BookCopies.FirstOrDefault()?.BookDescription.Title ==
                "Halo: Tales from Slipspace");
        }

        [TestMethod]
        public void TestSave_ShouldReturnRightInfo()
        {
            var condition = _dbTest.Conditions.FirstOrDefault();

            BookCopy bookCopy = new BookCopy
            {
                Condition = condition,
                BookDescription = new BookDescription
                {
                    Author = "Frank O'Connor",
                    Title = "Halo: Tales from Slipspace",
                    ISNB10 = "1506700721",
                    EAN = "9781506700724",
                    UPC = "asdfasd",
                    NbPage = 96,
                    DefaultPrice = 2599
                },
                User = _defaultUser,
                ReceivedDate = null,
                Canceled = null
            };

            Assert.IsTrue(!_dbTest.BookDescriptions.Any());

            var bookInfo = new BookBusiness(_dbTest, _sessionToken).Save(bookCopy);

            Assert.AreEqual(bookCopy.BookDescription.Title, bookInfo.Title);
            Assert.AreEqual(bookCopy.BookDescription.Author, bookInfo.Author);
            Assert.AreEqual(bookCopy.BookDescription.ISNB10, bookInfo.ISNB10);
            Assert.AreEqual(bookCopy.BookDescription.EAN, bookInfo.EAN);
            Assert.AreEqual(bookCopy.BookDescription.UPC, bookInfo.UPC);
            Assert.AreEqual(bookCopy.BookDescription.NbPage, bookInfo.NbPage);
            Assert.AreEqual(bookCopy.User.Email, bookInfo.UserEmail);
            Assert.AreEqual(bookCopy.User.PhoneNumber, bookInfo.UserPhoneNumber);
            Assert.AreEqual(bookCopy.User.UserId, bookInfo.UserId);
            Assert.AreEqual(bookInfo.CooperativeName, "Unassigned");
        }
    }
}
