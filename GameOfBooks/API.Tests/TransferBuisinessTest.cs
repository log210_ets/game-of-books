﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.BusinessLayer;
using API.Models;
using API.Tests.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace API.Tests
{

    [TestClass]
    class TransferBuisinessTest
    {
        [TestClass]
        public class BookBusinessTest
        {
            private readonly GobEntities _dbTest = new GobEntitiesTest();
            private readonly string _sessionToken1 = "sjdf2fd1f";
            private readonly string _sessionToken2 = "sjdfdfd1f";
            private Cooperative _coop1, _coop2;
            private User _coop1User, _coop2User;
            private BookCopy _defaultBookCopy1, _defaultBookCopy2, _defaultBookCopy3;

            private void ClearDatabase()
            {
                _dbTest.Database.ExecuteSqlCommand("delete from [ReservationDemands]");
                _dbTest.Database.ExecuteSqlCommand("delete from [Reservations]");
                _dbTest.Database.ExecuteSqlCommand("delete from [BookCopies]");
                _dbTest.Database.ExecuteSqlCommand("delete from [BookPrices]");
                _dbTest.Database.ExecuteSqlCommand("delete from [UserRoles]");
                _dbTest.Database.ExecuteSqlCommand("delete from [Users]");
                _dbTest.Database.ExecuteSqlCommand("delete from [Roles]");
                _dbTest.Database.ExecuteSqlCommand("delete from [BookDescriptions]");
                _dbTest.Database.ExecuteSqlCommand("delete from [Conditions]");
                _dbTest.Database.ExecuteSqlCommand("delete from [StringResources]");
                _dbTest.Database.ExecuteSqlCommand("delete from [Transfers]");
                _dbTest.Database.ExecuteSqlCommand("delete from [Cooperatives]");
                _dbTest.SaveChanges();
            }

            private User createNewUser(List<Role> roles, Cooperative coop, string token)
            {
                return new User
                {
                    Roles = new List<Role> { roles[0] },
                    Email = "julien_1dsd__a28@hotdmail.com",
                    Password = "123456",
                    SessionToken = token,
                    Cooperative = coop
                };
            }

            private BookCopy createNewBookCopy(Cooperative coop)
            {
                return new BookCopy
                {
                    Condition = null,
                    BookDescription = new BookDescription
                    {
                        Author = "Frank O'Connor",
                        Title = "Halo: Tales from Slipspace",
                        ISNB10 = "1506700721",
                        EAN = "9781506700724",
                        NbPage = 96,
                        DefaultPrice = 2599
                    },
                    User = _coop1User,
                    ReceivedDate = null,
                    Canceled = null,
                    Cooperative = coop

                };
            }

            private Reservation createNewReservation(Cooperative fromCoop, Cooperative toCoop, DateTime? receiptDate, Transfer tranfert, BookCopy bookCopy, User user)
            {
                return new Reservation
                {
                    Cooperative = fromCoop,
                    Cooperative1 = toCoop,
                    ReceiptDate = receiptDate,
                    Transfer = tranfert,
                    BookCopy = bookCopy,
                    User = user
                };
            }

            private Transfer createNewTransfert(List<Reservation> resevations)
            {
                return new Transfer
                {
                 ExpeditionDate = DateTime.Now,
                 Reservations = resevations
                };
            }

            [TestInitialize]
            public void InitializeTest()
            {

                ClearDatabase();

                List<Role> roles = new List<Role>
                {
                    new Role {Name = "Manager"}
                };

                _coop1 = new Cooperative { Name = "Coop1" };
                _coop2 = new Cooperative { Name = "Coop1" };

                _coop1User = createNewUser(roles, _coop1, _sessionToken1);
                _coop2User = createNewUser(roles, _coop2, _sessionToken2);

                _dbTest.Conditions.Add(new Condition { BookCondition = "Used" });
                _dbTest.Cooperatives.Add(_coop1);
                _dbTest.Cooperatives.Add(_coop2);
                _dbTest.Roles.AddRange(roles);
                _coop1User = _dbTest.Users.Add(_coop1User);
                _coop2User = _dbTest.Users.Add(_coop2User);

                _defaultBookCopy1 = createNewBookCopy(_coop1);
                _defaultBookCopy2 = createNewBookCopy(_coop1);
                _defaultBookCopy3 = createNewBookCopy(_coop1);


                _dbTest.BookCopies.Add(_defaultBookCopy1);
                _dbTest.BookCopies.Add(_defaultBookCopy2);
                _dbTest.BookCopies.Add(_defaultBookCopy3);
                _dbTest.SaveChanges();
            }

            [TestCleanup]
            public void CleanUp()
            {
                ClearDatabase();
            }


            [TestMethod]
            public void TestSearchBookToTransfer_Empty()
            {
                Assert.IsTrue(true);

                Reservation reservation1 = createNewReservation(_coop1, _coop2, null, new Transfer { ExpeditionDate = DateTime.Now }, _defaultBookCopy1, _coop1User);
                Reservation reservation2 = createNewReservation(_coop1, _coop2, DateTime.Now, null, _defaultBookCopy2, _coop1User);
                Reservation reservation3 = createNewReservation(_coop1, _coop1, null, null, _defaultBookCopy2, _coop1User);

                _dbTest.Reservations.Add(reservation1);
                _dbTest.Reservations.Add(reservation2);
                _dbTest.Reservations.Add(reservation3);
                _dbTest.SaveChanges();

                BookInfo[] booksInfo = new TransfertBusiness(_dbTest, _sessionToken1).SearchBookToTransfer();

                Assert.IsTrue(booksInfo.Count() == 0);

            }
            [TestMethod]
            public void TestSearchBookToTransfer_NotEmpty()
            {
                Assert.IsTrue(true);

                Reservation reservation1 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy1, _coop1User);

                _dbTest.Reservations.Add(reservation1);
                _dbTest.SaveChanges();

                BookInfo[] booksInfo = new TransfertBusiness(_dbTest, _sessionToken1).SearchBookToTransfer();

                Assert.IsTrue(booksInfo.Count() == 1);

            }

            [TestMethod]
            [ExpectedException(typeof(Exception), "reservations not found")]
            public void TestSave_ShouldThrow_MissingReservation()
            {
                new TransfertBusiness(_dbTest, _sessionToken1).CreateTransfer(null);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception), "reservations not found")]
            public void TestSave_ShouldThrow_MissingReservation2()
            {
                Reservations reservation = new Reservations();
                ICollection<ReservationId> lstReservationId = new List<ReservationId>();

                lstReservationId.Add(new ReservationId { value = -1 });
                reservation.reservations = lstReservationId;


                new TransfertBusiness(_dbTest, _sessionToken1).CreateTransfer(reservation);
            }

            [TestMethod]
            public void TestSave_Assert_Transfert()
            {
                Reservation reservation1 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy1, _coop1User);
                Reservation reservation2 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy2, _coop1User);

                _dbTest.Reservations.Add(reservation1);
                _dbTest.Reservations.Add(reservation2);
                _dbTest.SaveChanges();

                Reservations reservation = new Reservations();
                ICollection<ReservationId> lstReservationId = new List<ReservationId>();

                lstReservationId.Add(new ReservationId { value = reservation1.ReservationId });
                lstReservationId.Add(new ReservationId { value = reservation2.ReservationId });
                reservation.reservations = lstReservationId;

                new TransfertBusiness(_dbTest, _sessionToken1).CreateTransfer(reservation);


                Transfer transfert = _dbTest.Transfers.Include("Reservations").FirstOrDefault();

                Assert.IsTrue(transfert != null);
                Assert.IsTrue(transfert.Reservations.First().ReservationId == reservation1.ReservationId);
                Assert.IsTrue(transfert.Reservations.Skip(1).First().ReservationId == reservation2.ReservationId);

            }
            [TestMethod]
            public void TestSearch_BookToReceive_Empty()
            {
                Reservation reservation1 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy1, _coop1User);
                Reservation reservation2 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy2, _coop1User);

                _dbTest.Reservations.Add(reservation1);
                _dbTest.Reservations.Add(reservation2);
               
                List<Reservation> lstReservation = new List<Reservation>();
                lstReservation.Add(reservation1);
                lstReservation.Add(reservation2);

                Transfer tranfert = createNewTransfert(lstReservation);

                _dbTest.Transfers.Add(tranfert);
                _dbTest.SaveChanges();

                BookInfo[] booksInfo = new TransfertBusiness(_dbTest, _sessionToken1).SearchBookTransfer();

                Assert.IsTrue(booksInfo.Count() == 0);

            }

            [TestMethod]
            public void TestSearch_BookToReceive_notEmpty()
            {
                Reservation reservation1 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy1, _coop1User);
                Reservation reservation2 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy2, _coop1User);
                Reservation reservation3 = createNewReservation(new Cooperative { Name = "test" }, _coop2, null, null, _defaultBookCopy3, _coop1User);

                _dbTest.Reservations.Add(reservation1);
                _dbTest.Reservations.Add(reservation2);
                _dbTest.Reservations.Add(reservation3);

                _dbTest.SaveChanges();

                Reservations reservation = new Reservations();
                ICollection<ReservationId> lstReservationId = new List<ReservationId>();

                lstReservationId.Add(new ReservationId { value = reservation1.ReservationId });
                lstReservationId.Add(new ReservationId { value = reservation2.ReservationId });
                lstReservationId.Add(new ReservationId { value = reservation3.ReservationId });
                reservation.reservations = lstReservationId;

                _dbTest.SaveChanges();

                new TransfertBusiness(_dbTest, _sessionToken1).CreateTransfer(reservation);

             
                BookInfo[] booksInfo = new TransfertBusiness(_dbTest, _sessionToken2).SearchBookTransfer();

                Assert.IsTrue(booksInfo.Count() == 3);

                Assert.IsTrue(_dbTest.Transfers.Count() == 1);

            }

            [TestMethod]
            [ExpectedException(typeof(Exception), "reservations not found")]
            public void TestConfirmTransfer_ShouldThrow_MissingReservation()
            {
                new TransfertBusiness(_dbTest, _sessionToken1).ConfirmTransfer(null);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception), "reservations not found")]
            public void TestConfirmTransfer_ShouldThrow_MissingReservation2()
            {
                Reservations reservation = new Reservations();
                ICollection<ReservationId> lstReservationId = new List<ReservationId>();

                lstReservationId.Add(new ReservationId { value = -1 });
                reservation.reservations = lstReservationId;

                new TransfertBusiness(_dbTest, _sessionToken1).ConfirmTransfer(reservation);
            }

            [TestMethod]
            public void TestConfirmTransfer1()
            {
                Reservation reservation1 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy1, _coop1User);
                Reservation reservation2 = createNewReservation(_coop1, _coop2, null, null, _defaultBookCopy2, _coop1User);
                Reservation reservation3 = createNewReservation(new Cooperative { Name = "test" }, _coop2, null, null, _defaultBookCopy3, _coop1User);


                List<Reservation> resvations = new List<Reservation>();
                resvations.Add(reservation1);
                resvations.Add(reservation2);
                resvations.Add(reservation3);

                Transfer transfert = createNewTransfert(resvations);

                _dbTest.Transfers.Add(transfert);
                _dbTest.SaveChanges();


                Reservations reservation = new Reservations();
                ICollection<ReservationId> lstReservationId = new List<ReservationId>();

                lstReservationId.Add(new ReservationId { value = reservation1.ReservationId });
                lstReservationId.Add(new ReservationId { value = reservation2.ReservationId });
                lstReservationId.Add(new ReservationId { value = reservation3.ReservationId });
                reservation.reservations = lstReservationId;

                new TransfertBusiness(_dbTest, _sessionToken2).ConfirmTransfer(reservation);

                Assert.IsTrue(_dbTest.Transfers.First().ReceiptDate != null);


                Assert.IsTrue(_dbTest.Reservations.First().ExpirationDate != null);

            }
        }
    }
}
