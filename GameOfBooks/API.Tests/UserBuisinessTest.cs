﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.BusinessLayer;
using API.Models;
using API.Tests.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace API.Tests
{
    [TestClass]
    public class UserBuisinessTest
    {
        private readonly GobEntities _dbTest = new GobEntitiesTest();

        private void ClearDatabase()
        {

            _dbTest.Database.ExecuteSqlCommand("delete from [BookCopies]");
            _dbTest.Database.ExecuteSqlCommand("delete from [UserRoles]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Users]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Roles]");
            _dbTest.Database.ExecuteSqlCommand("delete from [BookPrices]");
            _dbTest.Database.ExecuteSqlCommand("delete from [BookDescriptions]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Conditions]");
            _dbTest.Database.ExecuteSqlCommand("delete from [Cooperatives]");
            _dbTest.Database.ExecuteSqlCommand("delete from [StringResources]");
            _dbTest.SaveChanges();
        }

        [TestInitialize]
        public void InitializeTest()
        {
            ClearDatabase();

            List<Role> roles = new List<Role>
            {
                new Role {Name = "defaultRole"},
                new Role {Name = "Manager"}
            };

            User defaultUser = new User
            {
                Roles = new List<Role> {roles[0]},
                Email = "julien128@hotmail.com",
                Password = "123456"
            };

            _dbTest.Roles.AddRange(roles);
            _dbTest.Users.Add(defaultUser);
            _dbTest.SaveChanges();
        }

        [TestCleanup]
        public void CleanUp()
        {
            ClearDatabase();
        }

        [TestMethod]
        public void TestLogin_ShouldReturnEmptyLoginResult_WithBadCriterias()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "1234567"
            });

            Assert.IsFalse(result.IsAuthentified);
            Assert.IsNull(result.SessionToken);
            Assert.IsNull(result.Roles);
        }

        [TestMethod]
        public void TestLogin_ShouldNotSaveSessionToken_WithBadCriterias()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "1234567"
            });

            var dbSessionToken = _dbTest.Users.FirstOrDefault(u => u.SessionToken == result.SessionToken)?.SessionToken;

            Assert.IsNull(dbSessionToken);
        }

        [TestMethod]
        public void TestLogin_ShouldReturnLoginResult_WithGoodCriterias()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "123456"
            });

            Assert.IsTrue(result.IsAuthentified);
            Assert.IsNotNull(result.SessionToken);
            Assert.IsNotNull(result.Roles);
            Assert.AreEqual(result.Roles.ElementAt(0), "defaultRole");
        }

        [TestMethod]
        public void TestLogin_ShouldSaveSessionToken_WithGoodCriterias()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "123456"
            });

            var dbSessionToken = _dbTest.Users.FirstOrDefault(u => u.SessionToken == result.SessionToken)?.SessionToken;
            
            Assert.IsNotNull(dbSessionToken);
            Assert.AreEqual(dbSessionToken, result.SessionToken);
        }

        [TestMethod]
        public void TestIsUserLoggedIn_ShouldReturnTrue()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "123456"
            });

            Assert.IsTrue(new UserBusiness(_dbTest, result.SessionToken).IsUserLoggedIn());
        }

        [TestMethod]
        public void TestIsUserLoggedIn_ShouldReturnFalse()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "1234567"
            });

            Assert.IsFalse(new UserBusiness(_dbTest, result.SessionToken).IsUserLoggedIn());
        }

        [TestMethod]
        public void TestGetLoggedUser_ShouldReturnLoggedUser()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "123456"
            });

            LoggedUser user = new UserBusiness(_dbTest, result.SessionToken).GetLoggedUser();

            Assert.IsNotNull(user);
            Assert.AreEqual(user.SessionToken, result.SessionToken);
            Assert.IsTrue(user.IsAuthentified);
            Assert.IsTrue(user.Roles.Any(r => result.Roles.Any(rr => rr == r)));
        }

        [TestMethod]
        public void TestGetLoggedUser_ShouldReturnEmptyShell()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "1234567"
            });

            LoggedUser user = new UserBusiness(_dbTest, result.SessionToken).GetLoggedUser();

            Assert.IsNotNull(user);
            Assert.IsNull(user.SessionToken);
            Assert.IsFalse(user.IsAuthentified);
            Assert.IsNull(user.Roles);
        }

        [TestMethod]
        public void TestLogout_ShouldRemoveSessionTokenInDb()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            var result = userBusiness.Login(new User
            {
                Email = "julien128@hotmail.com",
                Password = "123456"
            });

            Assert.IsTrue(new UserBusiness(_dbTest, result.SessionToken).IsUserLoggedIn());

            new UserBusiness(_dbTest, result.SessionToken).Logout();

            Assert.IsFalse(new UserBusiness(_dbTest, result.SessionToken).IsUserLoggedIn());
            Assert.IsNotNull(_dbTest.Users.FirstOrDefault(u => u.Email == "julien128@hotmail.com"));
            Assert.IsNull(_dbTest.Users.FirstOrDefault(u => u.Email == "julien128@hotmail.com")?.SessionToken);
        }

        [TestMethod]
        public void TestEmailAvailable_ShouldReturnTrue()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            Assert.IsTrue(userBusiness.EmailAvailable(new Email {email = "julien2@hotmail.com"}));
        }

        [TestMethod]
        public void TestEmailAvailable_ShouldReturnFalse()
        {
            UserBusiness userBusiness = new UserBusiness(_dbTest);
            Assert.IsFalse(userBusiness.EmailAvailable(new Email { email = "julien128@hotmail.com" }));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "User is null")]
        public void TestSave_ShouldThrowException_WithUserEqualsNull()
        {
            new UserBusiness(_dbTest).Save(null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "User already exists")]
        public void TestSave_ShouldThrowException_WithExistingUser()
        {
            User user = new User
            {
                Roles = null,
                Email = "julien128@hotmail.com",
                Password = "123456"
            };

            new UserBusiness(_dbTest).Save(user);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "No role selected")]
        public void TestSave_ShouldThrowException_WithoutRole()
        {
            User user = new User
            {
                Roles = null,
                Email = "j8@hotmail.com",
                Password = "123456"
            };

            new UserBusiness(_dbTest).Save(user);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Email is mandatory")]
        public void TestSave_ShouldThrowException_WithoutEmail()
        {
            User user = new User
            {
                Roles = new List<Role> { new Role { Name = "Manager" } },
                Email = null,
                Password = "123456"
            };

            new UserBusiness(_dbTest).Save(user);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Role doesnt exists")]
        public void TestSave_ShouldThrowException_WithUnvalidRole()
        {
            User user = new User
            {
                Roles = new List<Role> { new Role { Name = "FOO" } },
                Email = "j8@hotmail.com",
                Password = "123456"
            };

            new UserBusiness(_dbTest).Save(user);
        }

        [TestMethod]
        public void TestSave_ShoulCreateCoop_IfManagerAndCoopNotExists()
        {
            User user = new User
            {
                Roles = new List<Role> { new Role { Name = "Manager" } },
                Email = "j8@hotmail.com",
                Password = "123456",
                Cooperative = new Cooperative { Address = "fakeAddress", Name = "fakeName"}
            };

            new UserBusiness(_dbTest).Save(user);

            var userInCoop = _dbTest.Cooperatives.FirstOrDefault(c => c.Name == "fakeName")?
                .Users.Any(u => u.Email == "j8@hotmail.com");

            Assert.IsNotNull(_dbTest.Cooperatives.FirstOrDefault(c => c.Name == "fakeName"));
            Assert.IsTrue(userInCoop.HasValue);
            Assert.IsTrue((bool)userInCoop);
        }
        
        [TestMethod]
        public void TestSave_ShoulNotCreateCoop_IfExists()
        {
            _dbTest.Cooperatives.Add(new Cooperative {Address = "fakeAddress", Name = "fakeName"});
            _dbTest.SaveChanges();

            User user = new User
            {
                Roles = new List<Role> { new Role { Name = "Manager" } },
                Email = "j8@hotmail.com",
                Password = "123456",
                Cooperative = new Cooperative { Address = "fakeAddress", Name = "fakeName" }
            };


            new UserBusiness(_dbTest).Save(user);

            var userInCoop = _dbTest.Cooperatives.FirstOrDefault(c => c.Name == "fakeName")?
                .Users.Any(u => u.Email == "j8@hotmail.com");

            Assert.IsNotNull(_dbTest.Cooperatives.FirstOrDefault(c => c.Name == "fakeName"));
            Assert.AreEqual(_dbTest.Cooperatives.Count(), 1);
        }

        [TestMethod]
        public void TestSave_RegistrationDate_ShouldBeToday()
        {
            User user = new User
            {
                Roles = new List<Role> { new Role { Name = "Manager" } },
                Email = "j8@hotmail.com",
                Password = "123456",
                Cooperative = new Cooperative { Address = "fakeAddress", Name = "fakeName" }
            };


            new UserBusiness(_dbTest).Save(user);

            Assert.IsTrue(_dbTest.Users.FirstOrDefault(u => u.Email == "j8@hotmail.com")?.RegistrationDate.Date ==
                          DateTime.Now.Date);
        }

        [TestMethod]
        public void TestSave_UserShouldBeSavedInDatabase()
        {
            User user = new User
            {
                Roles = new List<Role> { new Role { Name = "Manager" } },
                Email = "j8@hotmail.com",
                Password = "123456",
                Cooperative = new Cooperative { Address = "fakeAddress", Name = "fakeName" }
            };


            new UserBusiness(_dbTest).Save(user);

            Assert.IsTrue(_dbTest.Users.Any(u => u.Email == "j8@hotmail.com"));
        }
    }
}
