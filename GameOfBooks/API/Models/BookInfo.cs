﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class BookInfo
    {
        public int? BookCopyId { get; set; }

        public int? UserId { get; set; }
        public string UserEmail { get; set; }
        public string UserPhoneNumber { get; set; }

        public int? BookDescriptionId { get; set; }
        public string ISNB10 { get; set; }
        public string EAN { get; set; }
        public string UPC { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int? NbPage { get; set; }
        public decimal? DefaultPrice { get; set; }
        public decimal? SellingPrice { get; set; }


        public int? CooperativeId { get; set; }
        public int? DestinationCooperativeId { get; set; }
        public string CooperativeName { get; set; }
        public string CooperativeAddress { get; set; }
        public decimal? CooperativePrice { get; set; }

        public int ConditionId { get; set; }
        public string BookCondition { get; set; }

        public int? ReservationId { get; set; }

        public int? TransfertId { get; set; }

        public string TransfertDate { get; set; }

        public string ebayItemID { get; set; }
        public DateTime? EbayAddedDate { get; set; }

    }
}