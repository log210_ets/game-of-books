﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class LoginResult
    {
        public bool IsAuthentified { get; set; }
        public string SessionToken { get; set; }
        public string[] Roles { get; set; }
    }
}