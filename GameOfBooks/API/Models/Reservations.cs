﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class Reservations
    {
        public virtual ICollection<ReservationId> reservations { get; set; }
    }

    public class ReservationId
    {
        public int value { get; set; }
    }
}