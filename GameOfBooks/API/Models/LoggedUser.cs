﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class LoggedUser
    {
        public string Email { get; set; }
        public string SessionToken { get; set; }
        public string[] Roles { get; set; }
        public bool IsAuthentified { get; set; }
    }
}