//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookPrice
    {
        public int CooperativeId { get; set; }
        public int BookDescriptionId { get; set; }
        public Nullable<decimal> CoopPrice { get; set; }
    
        public virtual BookDescription BookDescription { get; set; }
        public virtual Cooperative Cooperative { get; set; }
    }
}
