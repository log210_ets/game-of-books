﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class UserMetadata
    {

        [StringLength(250)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(250)]
        public string Email { get; set; }

        [Required]
        public System.DateTime RegistrationDate { get; set; }
    }

    public class CooperativeMetadata
    {

        [Required]
        [StringLength(250)]
        public string Name { get; set; }
    }
}