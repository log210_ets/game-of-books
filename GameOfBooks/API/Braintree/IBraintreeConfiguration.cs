﻿using Braintree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Braintree
{
    public interface IBraintreeConfiguration
    {
        IBraintreeGateway CreateGateway();
        string GetConfigurationSetting(string setting);
        IBraintreeGateway GetGateway();
    }
}