﻿using API.Models;
using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using API.Tools;

namespace API.EbaySDK
{
    public class EbaySDK
    {
        private static ApiContext _apiContext;
        private static readonly GobEntities Db = new GobEntities();

        public static BookCopy AddItem(double sellingPrice, int bookCopyId)
        {
            BookCopy bookCopy = Db.BookCopies
                            .Include(b => b.Condition)
                            .Include(p => p.Cooperative.BookPrices)
                            .Include(d => d.BookDescription)
                            .FirstOrDefault(x => x.BookCopyId == bookCopyId);
            try
            {
                //[Step 1] Initialize eBay ApiContext object
                ApiContext apiContext = GetApiContext();

                //[Step 2] Create a new ItemType object
                ItemType item = BuildItem(bookCopy, sellingPrice);


                //[Step 3] Create Call object and execute the Call
                AddItemCall apiCall = new AddItemCall(apiContext);
                Console.WriteLine("Begin to call eBay API, please wait ...");
                FeeTypeCollection fees = apiCall.AddItem(item);
                Console.WriteLine("End to call eBay API, show call result ...");
                Console.WriteLine();

                //[Step 4] Handle the result returned
                Console.WriteLine("The item was listed successfully!");
                double listingFee = 0.0;
                foreach (FeeType fee in fees)
                {
                    if (fee.Name == "ListingFee")
                    {
                        listingFee = fee.Fee.Value;
                    }
                }

                //[Step 5] Add the infos to the Dababase
                Ebay ebayDB = new Ebay();
                ebayDB.BookCopyId = bookCopyId;
                ebayDB.EbayItemID = item.ItemID;
                ebayDB.AddedDate = DateTime.Now;
                Db.Ebays.Add(ebayDB);

                Db.SaveChanges();

                return bookCopy;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fail to list the item : " + ex.Message);
                return bookCopy;
            }
        }

        /// <summary>
        /// Remove an item from ebay.
        /// </summary>
        /// <param name="ebayItemId">The ebay item ID.</param>
        public static BookCopy RemoveItem(String ebayItemId)
        {
            BookCopy bookCopy = Db.BookCopies.Include(c => c.Ebay).FirstOrDefault(x => x.Ebay.EbayItemID == ebayItemId);
            try
            {
                //[Step 1] Initialize eBay ApiContext object
                ApiContext apiContext = GetApiContext();
                
                if (bookCopy != null)
                {
                    //[Step 2] Remove item from eBay
                    EndItemCall apicall = new EndItemCall(apiContext);

                    apicall.EndItem(bookCopy.Ebay.EbayItemID, EndReasonCodeType.NotAvailable);

                    //[Step 3] Remove the item from the database
                    Db.Ebays.Remove(bookCopy.Ebay);
                    Db.SaveChanges();

                    return bookCopy;
                }
                else
                    throw new Exception("Could not find the item!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not change the state of the item, error : " + ex.Message);
                return bookCopy;
            }
        }

        /// <summary>
        /// Populate eBay SDK ApiContext object with data from application configuration file
        /// </summary>
        /// <returns>ApiContext object</returns>
        private static ApiContext GetApiContext()
        {
            //apiContext is a singleton,
            //to avoid duplicate configuration reading
            if (_apiContext != null)
            {
                return _apiContext;
            }
            else
            {
                _apiContext = new ApiContext();

                //set Api Server Url
                _apiContext.SoapApiServerUrl =
                    ConfigurationManager.AppSettings["Environment.ApiServerUrl"];
                //set Api Token to access eBay Api Server
                ApiCredential apiCredential = new ApiCredential();
                apiCredential.eBayToken =
                    ConfigurationManager.AppSettings["UserAccount.ApiToken"];
                _apiContext.ApiCredential = apiCredential;
                //set eBay Site target to Canada
                _apiContext.Site = SiteCodeType.Canada;

                //set Api logging
                _apiContext.ApiLogManager = new ApiLogManager();
                _apiContext.ApiLogManager.ApiLoggerList.Add(
                    new FileLogger("listing_log.txt", true, true, true)
                    );
                _apiContext.ApiLogManager.EnableLogging = true;

                return _apiContext;
            }
        }

        /// <summary>
        /// Build a sample item
        /// </summary>
        /// <returns>ItemType object</returns>
        private static ItemType BuildItem(BookCopy bookCopy, double sellingPrice)
        {
            ItemType item = new ItemType();

            // item title
            item.Title = bookCopy.BookDescription.Title;
            // item description
            item.Description = bookCopy.BookDescription.Title + " by " + bookCopy.BookDescription.Author + ".";

            // listing type
            item.ListingType = ListingTypeCodeType.FixedPriceItem;
            // listing price
            item.Currency = CurrencyCodeType.CAD;
            item.StartPrice = new AmountType();

            // Get the price of the item

            double? price = sellingPrice;

            item.StartPrice.Value = price.Value;
            item.StartPrice.currencyID = CurrencyCodeType.CAD;

            // listing duration
            item.ListingDuration = "Days_7";

            // item location and country
            item.Location = "Montreal";
            item.Country = CountryCodeType.CA;

            // listing category, 
            CategoryType category = new CategoryType();
            category.CategoryID = "2228"; //CategoryID = 11104 (Textbooks, Education) , Parent CategoryID=267(Books)
            item.PrimaryCategory = category;

            // item quality
            item.Quantity = 1;

            // item condition, New
            //item.ConditionID = 1000;

            // item condition, Used
            item.ConditionID = 3000;

            // payment methods
            item.PaymentMethods = new BuyerPaymentMethodCodeTypeCollection();
            item.PaymentMethods.AddRange(
                new BuyerPaymentMethodCodeType[] { BuyerPaymentMethodCodeType.PayPal }
                );
            // email is required if paypal is used as payment method
            item.PayPalEmailAddress = "marc.andre.landry@hotmail.com";

            // handling time is required
            item.DispatchTimeMax = 1;
            // shipping details
            item.ShippingDetails = BuildShippingDetails();

            // return policy
            item.ReturnPolicy = new ReturnPolicyType();
            item.ReturnPolicy.ReturnsAcceptedOption = "ReturnsAccepted";

            return item;
        }

        /// <summary>
        /// Build sample shipping details
        /// </summary>
        /// <returns>ShippingDetailsType object</returns>
        private static ShippingDetailsType BuildShippingDetails()
        {
            // Shipping details
            ShippingDetailsType sd = new ShippingDetailsType();

            sd.ApplyShippingDiscount = true;
            AmountType amount = new AmountType();
            amount.Value = 2.80;
            amount.currencyID = CurrencyCodeType.CAD;
            sd.PaymentInstructions = "eBay .Net SDK test instruction.";

            // Shipping type and shipping service options
            sd.ShippingType = ShippingTypeCodeType.Flat;
            ShippingServiceOptionsType shippingOptions = new ShippingServiceOptionsType();

            shippingOptions.ShippingService =
                ShippingServiceCodeType.CA_PostRegularParcel.ToString();
            amount = new AmountType();
            amount.Value = 2.0;
            amount.currencyID = CurrencyCodeType.CAD;

            shippingOptions.ShippingServiceAdditionalCost = amount;
            amount = new AmountType();
            amount.Value = 1;
            amount.currencyID = CurrencyCodeType.CAD;

            shippingOptions.ShippingServiceCost = amount;
            shippingOptions.ShippingServicePriority = 1;
            amount = new AmountType();
            amount.Value = 1.0;
            amount.currencyID = CurrencyCodeType.CAD;

            shippingOptions.ShippingInsuranceCost = amount;

            sd.ShippingServiceOptions = new ShippingServiceOptionsTypeCollection(
                new ShippingServiceOptionsType[] { shippingOptions }
                );

            return sd;
        }
    }
}