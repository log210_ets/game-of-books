﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using API.Models;

namespace API.Tools
{
    public class Email : InvoiceManager
    {
        public Email(BookInfo bookInfo, User toNotify) : base(bookInfo, toNotify.Email, toNotify.PhoneNumber)
        {
        }

        public Email(BookInfo bookInfo,  string email, string phone) : base(bookInfo, email, phone)
        {
        }

        private static bool Send(MailAddress sendTo, MailAddress sendFrom, MailAddress[] cc, string subject, string body)
        {
            var client = new SmtpClient();
            var msg = new MailMessage();
            MemoryStream memStream = null;
            string ppp = "AXYlwipa9TwvyNiFCY1nMw==";
            byte[] key = {};
            byte[] IV = {12, 21, 43, 17, 57, 35, 67, 27};
            var e = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(e);
            var byteInput = new byte[ppp.Length];
            byteInput = Convert.FromBase64String(ppp);
            var provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            var transform = provider.CreateDecryptor(key, IV);
            var cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();

            NetworkCredential smtpCreds = null;
            var encoding1 = Encoding.UTF8;
            smtpCreds = new NetworkCredential("game.of.books.ets@gmail.com", encoding1.GetString(memStream.ToArray()));
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.Credentials = smtpCreds;
            client.EnableSsl = true;

            msg.Subject = subject;
            msg.Body = body;
            msg.From = sendFrom;
            msg.To.Add(sendTo);

            client.Send(msg);
            return true;
        }

        protected override void PerformInvoice()
        {
            var from = new MailAddress("game.of.books.ets@gmail.com", "Game of books");
            var to = new MailAddress(Email);
            string subject = Title;
            string message = DescriptionBody;

            Send(to, from, null, subject, message);
        }
    }
}