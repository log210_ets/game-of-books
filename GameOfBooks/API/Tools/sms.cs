﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Twilio;

namespace API.Tools
{
    public class Sms : InvoiceManager
    {
        private const bool  USE_SMS = true;
        public Sms(BookInfo bookInfo, User toNotify) : base(bookInfo, toNotify.Email, toNotify.PhoneNumber)
        {
        }

        public Sms(BookInfo bookInfo, string email, string phone) : base(bookInfo, email, phone)
        {
        }

        protected override void PerformInvoice()
        {

            if (!USE_SMS)
                return;



            // Find your Account Sid and Auth Token at twilio.com/user/account
            string AccountSid = "AC5dd4860107e872524888be0628e3e19d";
            string AuthToken = "c461c1dcb444cf0bd09e3a01c2a51801";
            var twilio = new TwilioRestClient(AccountSid, AuthToken);


            var message = twilio.SendMessage(
                "+14509153423", "+1" + Telephone,
               DescriptionBody + Title);

            Console.WriteLine(message.Sid);
        }
    }
}