﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using API.BusinessLayer;
using API.Models;

namespace API.Tools
{
    public static class BookInfoConverter
    {
        public static IQueryable<BookCopy> GetConvertableBookCopies(gobEntities db)
        {
            return db.BookCopies
                .Include(b => b.BookDescription)
                .Include(b => b.Cooperative)
                .Include(b => b.Cooperative.BookPrices)
                .Include(b => b.Condition)
                .Include(b => b.Ebay)
                .Include(b => b.Reservations)
                .Include(b => b.Reservations.Select(r => r.User))
                .Include(b => b.Reservations.Select(r => r.Transfer));


        }

        public static decimal? GetCoopPrice(User user,BookCopy bookCopy)
        {
            decimal? coopPrice = null;

            // A user allowed to use this method is a Manager, and the manager manage a specified coop
            // so we have to load the price from the user's coop
            if (user.Roles.Any(r => r.Name == "Manager" || r.Name == "Dev"))
            {
                coopPrice = user.Cooperative?.BookPrices?.FirstOrDefault(
                    bc => bc.BookDescriptionId == bookCopy.BookDescription?.BookDescriptionId)?.CoopPrice;
            }
            else //A student can only see the price setted by the coop that sells the bookCopy
            {
                coopPrice =
                    bookCopy.Cooperative.BookPrices.FirstOrDefault(
                        b => b.BookDescriptionId == bookCopy.BookDescriptionId)?.CoopPrice;
            }

            return coopPrice;
        }

        public static decimal CalculateSellingPrice(BookCopy bookCopy, decimal? coopPrice)
        {

            decimal? sellingPrice = -1;
            var factor = bookCopy.Condition?.PriceFactor;
            if (factor != null)
            {
                sellingPrice = coopPrice ?? bookCopy.BookDescription?.DefaultPrice;
                sellingPrice *= (decimal)factor;

                if (sellingPrice != null)
                {
                    var hasSupplement = bookCopy.Reservations.Any(r => r.Transfer?.ReceiptDate == null);
                    sellingPrice += hasSupplement ? 10 : 0;
                }
            }
            return sellingPrice??-1;
        }

        public static BookInfo ToBookInfo(User user, BookCopy bookCopy, Reservation reservation = null, Transfer transfer = null)
        {
            decimal? coopPrice = GetCoopPrice(user, bookCopy);
            decimal? sellingPrice = CalculateSellingPrice(bookCopy, coopPrice);

            return new BookInfo
            {
                BookCopyId = bookCopy.BookCopyId,

                UserId = bookCopy.User?.UserId,
                UserEmail = bookCopy.User?.Email,
                UserPhoneNumber = bookCopy.User?.PhoneNumber,

                BookDescriptionId = bookCopy.BookDescriptionId,
                ISNB10 = bookCopy.BookDescription?.ISNB10,
                EAN = bookCopy.BookDescription?.EAN,
                UPC = bookCopy.BookDescription?.UPC,
                Title = bookCopy.BookDescription?.Title,
                Author = bookCopy.BookDescription?.Author,
                NbPage = bookCopy.BookDescription?.NbPage,
                DefaultPrice = bookCopy.BookDescription?.DefaultPrice,

                DestinationCooperativeId = null,
                CooperativeId = bookCopy.CooperativeId,
                CooperativeName = reservation?.Cooperative1?.Name ?? bookCopy.Cooperative?.Name,
                CooperativeAddress = bookCopy.Cooperative?.Address,
                CooperativePrice = coopPrice,
                SellingPrice = sellingPrice,

                BookCondition = bookCopy.Condition?.BookCondition,
                ConditionId = bookCopy.ConditionId,
                ReservationId = reservation?.ReservationId ?? bookCopy.Reservations.FirstOrDefault(r => r.ExpirationDate > DateTime.Now && r.ReceiptDate == null)?.ReservationId,
                TransfertDate = transfer?.ReceiptDate.ToString(),
                TransfertId = transfer?.TransferId,
                ebayItemID = bookCopy.Ebay?.EbayItemID,
                EbayAddedDate = bookCopy.Ebay?.AddedDate
            };
        }
    }
}