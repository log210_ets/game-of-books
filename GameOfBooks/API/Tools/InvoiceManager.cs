﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Tools
{
    public abstract class InvoiceManager
    {
        //TODO : GLOBALIZATION?
        private const string BOOK_RECEPTION_TILE = "Book receipt confirmation";
        private string _BookReceptionBody = "Your book: {0} written by {1} is now for sale by the cooperative: {2} at {3}";

        private const string BOOK_RESERVATION_TILE = "Book reservation request";
        private string _BookReservationBody = "A book: {0} written by {1} is now for sale by the cooperative: {2} at {3} \nYou can create a reservation";

        private const string CANCEL_RESERVATION_TILE = "Book transaction canceled";
        private string _CancelReservationBody = "Your reservation for the  book: {0} written by {1} has been canceled with success. \n You will receive a refund.";

        private const string TRANSFERT_RECEPTION_TILE = "Your book has arrived";
        private string _TransfertReceptionBody = "Your reservation for the  book: {0} written by {1} has arrived at the cooperative: {2}. \n You have 48 hours for the book récupération";

        private const string RESERVATION_DEMAND = "Your book is now available to reserve";
        private string _ReservationDemandBody = "Your demand for the  book: {0} written by {1} is now available at the cooperative: {2}. \n You can now perform a reservation";


        private readonly BookInfo _bookInfo;

        protected string Telephone;
        protected string Email;
        protected string Title;
        protected string DescriptionBody;


        protected InvoiceManager(BookInfo bookInfo, string email, string phone)
        {
            _bookInfo = bookInfo;
            Telephone = phone;
            Email = email;
        }

        protected abstract void PerformInvoice();



        public  void SendBookReception()
        {
            Title = BOOK_RECEPTION_TILE;
            DescriptionBody = String.Format(_BookReservationBody, _bookInfo.Title, _bookInfo.Author, _bookInfo.CooperativeName, _bookInfo.CooperativeAddress);

            PerformInvoice();
        }

        public void SendReservation()
        {
            Title = BOOK_RESERVATION_TILE;
            DescriptionBody = String.Format(_BookReservationBody, _bookInfo.Title, _bookInfo.Author, _bookInfo.CooperativeName, _bookInfo.CooperativeAddress);

            PerformInvoice();
        }

        public void SendReservationDemand()
        {
            Title = RESERVATION_DEMAND;
            DescriptionBody = String.Format(_ReservationDemandBody, _bookInfo.Title, _bookInfo.Author, _bookInfo.CooperativeName, _bookInfo.CooperativeAddress);

            PerformInvoice();
        }

        public void SendCancelTransaction()
        {
            Title = CANCEL_RESERVATION_TILE;
            DescriptionBody = String.Format(_CancelReservationBody, _bookInfo.Title, _bookInfo.Author);

            PerformInvoice();
        }

        // CU08, Le système envoie un courriel pour indiquer à l’étudiant que la coopérative vient de recevoir son ou ses livres et qu’il a 48 heures pour en faire la récupération.

        public void SendTransfertReception()
        {
            Title = TRANSFERT_RECEPTION_TILE;
            DescriptionBody = String.Format(_TransfertReceptionBody, _bookInfo.Title, _bookInfo.Author,_bookInfo.CooperativeName);

            PerformInvoice();
        }

    }
}