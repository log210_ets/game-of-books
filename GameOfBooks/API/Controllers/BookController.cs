﻿using API.Amazon;
using API.Models;
using Newtonsoft.Json;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Services;
using System.Web.Services;
using API.Attributes;
using API.BusinessLayer;
using API.BusinessLayer.BookSearch;


namespace API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BookController : ApiController
    {
        private GobEntities _db = new GobEntities();

        public IHttpActionResult Get()
        {
            return Ok(_db.BookCopies);
        }

        [Route("api/book/{id}")]
        public IHttpActionResult Get(int id)
        {
            BookCopy book = new BookBusiness(_db).GetBookCopy(id);
            
            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        [GobAuthorize(new[] {"Student"})]
        [HttpPost]
        public IHttpActionResult Save(BookCopy book)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).Save(book)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] {"Manager"})]
        [HttpPost]
        public IHttpActionResult EditBookCondition(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .EditBookCondition(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Student" })]
        [HttpPost]
        public IHttpActionResult RequestNotification(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .RequestNotification(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult EditCoopPrice(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .EditCoopPrice(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult ConfirmReceive(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .ConfirmReceive(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult CancelReceive(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .CancelReceive(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Student" })]
        [HttpPost]
        public IHttpActionResult ConfirmReservation(TransactionBraintree transactionBraintree)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .ConfirmReservation(transactionBraintree)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult ConfirmRecovery(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .ConfirmRecovery(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult CancelRecovery(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .CancelRecovery(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult addToEbay(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .AddToEbay(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult removeFromEbay(BookInfo bookInfo)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .RemoveFromEbay(bookInfo)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Student" })]
        public IHttpActionResult SearchBook(BookSearch search)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .SearchBookDescription(search)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        public IHttpActionResult SearchBookToReceive(BookSearch search)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookReceiveSearch(_db, ActionContext.Request.Headers.Authorization.ToString(), search)
                                .Search()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Student" })]
        public IHttpActionResult SearchBookToReserve(BookSearch search)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookReserveSearch(_db, ActionContext.Request.Headers.Authorization.ToString(), search)
                                .Search()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        public IHttpActionResult SearchBookToRecover(BookSearch search)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookRecoverSearch(_db, ActionContext.Request.Headers.Authorization.ToString(), search).Search()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [GobAuthorize(new[] { "Manager" })]
        public IHttpActionResult SearchInventory()
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new BookInventorySearch(_db, ActionContext.Request.Headers.Authorization.ToString()).Search()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult SearchBookToTransfer()
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new TransfertBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).SearchBookToTransfer()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public IHttpActionResult CreateTransfer(Reservations reservations)
        {
            try
            {

                new TransfertBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                 .CreateTransfer(reservations);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult SearchTransfer()
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new TransfertBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                .SearchBookTransfer()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Manager" })]
        [HttpPost]
        public IHttpActionResult ConfirmTransfer(Reservations reservations)
        {
            try
            {
                new TransfertBusiness(_db, ActionContext.Request.Headers.Authorization.ToString())
                                  .ConfirmTransfer(reservations);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);


            }
        }


        // Overide dispose in case of crash
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}