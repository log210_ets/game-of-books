﻿using System;
using API.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using API.Attributes;
using API.BusinessLayer;
using Newtonsoft.Json;


namespace API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {

        private readonly GobEntities _db = new GobEntities();

        public IHttpActionResult Login(User user)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new UserBusiness(_db, ActionContext.Request.Headers.Authorization?.ToString()).Login(user)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult IsUserLoggedIn()
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new UserBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).IsUserLoggedIn()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult GetLoggedUser()
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new UserBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).GetLoggedUser()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [GobAuthorize(new[] { "Student", "Manager" })]
        public IHttpActionResult Logout()
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new UserBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).Logout()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult EmailAvailable(Email email)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new UserBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).EmailAvailable(email)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Save(User user)
        {
            try
            {
                return
                    Ok(
                        JsonConvert.SerializeObject(
                            new UserBusiness(_db, ActionContext.Request.Headers.Authorization.ToString()).Save(user)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        

        // Overide dispose in case of crash
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
