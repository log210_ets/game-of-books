﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TranslateController : ApiController
    {
        private readonly GobEntities _db = new GobEntities();
        
        public IHttpActionResult Get()
        {
            return Ok(_db.StringResources);
        }
    }
}
