﻿using API.Braintree;
using API.Models;
using Braintree;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class CheckoutController : ApiController
    {
        public IBraintreeConfiguration config = new BraintreeConfiguration();
        private readonly GobEntities _db = new GobEntities();

        public static readonly TransactionStatus[] transactionSuccessStatuses = {
                                                                                    TransactionStatus.AUTHORIZED,
                                                                                    TransactionStatus.AUTHORIZING,
                                                                                    TransactionStatus.SETTLED,
                                                                                    TransactionStatus.SETTLING,
                                                                                    TransactionStatus.SETTLEMENT_CONFIRMED,
                                                                                    TransactionStatus.SETTLEMENT_PENDING,
                                                                                    TransactionStatus.SUBMITTED_FOR_SETTLEMENT
                                                                                };

        [Route("api/checkout/")]
        public IHttpActionResult Get()
        {
            var gateway = config.GetGateway();
            var clientToken = gateway.ClientToken.generate();

            return Ok(clientToken);
        }

        public bool Transaction(double itemPrice, Reservation reservation, string nonce)
        {
            try
            {
                var gateway = config.GetGateway();

                var request = new TransactionRequest
                {
                    Amount = (decimal) Math.Round(itemPrice, 2),
                    PaymentMethodNonce = nonce,
                    Options = new TransactionOptionsRequest
                    {
                        SubmitForSettlement = true
                    }
                };

                Result<Transaction> result = gateway.Transaction.Sale(request);

                if (result.IsSuccess()) {
                    _db.Braintrees.Add(new Models.Braintree
                    {
                        ReservationId = reservation.ReservationId,
                        BraintreeOrderID = result.Target.Id
                    });
                    _db.SaveChanges();
                    return true;
                }
                else {
                    return false;
                }
            } catch (Exception ex)
            {
                return false;
            }
        }
    }
}