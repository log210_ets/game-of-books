﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CooperativeController : ApiController
    {
        private GobEntities _db = new GobEntities();

        public IHttpActionResult Get()
        {
            return Ok(_db.Cooperatives.OrderBy(c => c.Name).Where(c => c.Name != "Unassigned"));
        }

        // Overide dispose in case of crash
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
