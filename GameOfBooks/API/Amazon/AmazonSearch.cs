﻿using API.Models;
using Nager.AmazonProductAdvertising;
using Nager.AmazonProductAdvertising.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace API.Amazon
{
    //https://github.com/tinohager/Nager.AmazonProductAdvertising
    public class AmazonSearch
    {
        public BookDescription ItemLookupRequest(string searchItem)
        {
            var authentication = new AmazonAuthentication();
            authentication.AccessKey = "AKIAINUH5Q3WAQ4UHLUA";
            authentication.SecretKey = "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";


            var wrapper = new AmazonWrapper(authentication, AmazonEndpoint.CA, "httpmonsica-20");
            var result = wrapper.Lookup(searchItem);

            if (result.Items.Item != null)
            {
                var amazonBook = result.Items.Item[0];
                var attributs = amazonBook.ItemAttributes;

                BookDescription book = new BookDescription();
                try
                {
                    book.Author = attributs.Author.First();
                    book.NbPage = int.Parse(attributs.NumberOfPages);
                    book.Title = attributs.Title;
                    book.ISNB10 = attributs.ISBN;
                    book.EAN    = attributs.EAN;
                    book.UPC = attributs.UPC;

                    var doubleArray = Regex.Split(attributs.ListPrice.FormattedPrice, @"[^0-9\.]+").Where(c => c != "." && c.Trim() != "");
                    book.DefaultPrice = decimal.Parse(doubleArray.First());
                }
                catch (Exception)
                {

                    return null;
                }
                return book;
            }
            return null;
        }
    }
}