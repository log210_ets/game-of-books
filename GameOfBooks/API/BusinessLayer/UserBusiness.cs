﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using API.Models;

namespace API.BusinessLayer
{
    public class UserBusiness
    {
        private readonly GobEntities _db;
        private readonly string _sessionToken;

        public UserBusiness(GobEntities db, string sessionToken = "")
        {
            _db = db;
            _sessionToken = sessionToken;
        }

        public static User GetCurrentUser(GobEntities db, string sessionToken)
        {
            var user =
                db.Users.Include(u => u.Cooperative.BookPrices).Include(r => r.Roles).FirstOrDefault(u => u.SessionToken == sessionToken);

            if (user == null)
                throw new Exception("User is not logged in");

            return user;
        }

        public LoginResult Login(User user)
        {
            LoginResult loginResult;
            if (_db.Users.Any(u => u.Email == user.Email && u.Password == user.Password))
            {
                var existingUser = _db.Users.Include("Roles").FirstOrDefault(u => u.Email == user.Email);
                Guid sessionToken = Guid.NewGuid();
                if (existingUser != null) existingUser.SessionToken = sessionToken.ToString();
                _db.SaveChanges();

                loginResult = new LoginResult
                {
                    IsAuthentified = true,
                    SessionToken = sessionToken.ToString(),
                    Roles = existingUser?.Roles.Select(r => r.Name).ToArray()
                };
            }
            else
                loginResult = new LoginResult { IsAuthentified = false, SessionToken = null };

            return loginResult;
        }


        public bool IsUserLoggedIn()
        {
            var isLoggedIn = _db.Users.Any(u => u.SessionToken == _sessionToken && u.SessionToken != null);
            return isLoggedIn;
        }

        public LoggedUser GetLoggedUser()
        {
            var user = _db.Users.Include("Roles").FirstOrDefault(u => u.SessionToken == _sessionToken && u.SessionToken != null);
            LoggedUser loggedUser = new LoggedUser
            {
                Email = user?.Email,
                Roles = user?.Roles.Select(r => r.Name).ToArray(),
                SessionToken = user?.SessionToken,
                IsAuthentified = user != null
            };
            return loggedUser;
        }

        public bool Logout()
        {
            var user = _db.Users.FirstOrDefault(u => u.SessionToken == _sessionToken);

            if (user != null)
            {
                user.SessionToken = null;
                _db.SaveChanges();
            }

            return true;
        }

        public bool EmailAvailable(Email email)
        {
            if (email == null || string.IsNullOrWhiteSpace(email.email))
                return false;

            var result = !_db.Users.Any(u => u.Email == email.email);
            return result;
        }

        public bool Save(User user)
        {
            if (user == null)
                throw new Exception("User is null");

            if (user.Email == null)
                throw new Exception("Email is mandatory");

            if (_db.Users.Any(u => u.Email == user.Email))
                throw new Exception("User already exists");
            
            if (user.Roles == null || user.Roles?.Count == 0)
                throw new Exception("No role selected");

            //TODO : implement multiple role on register....
            Role role = user.Roles.FirstOrDefault();
            
            var dbRole = _db.Roles.FirstOrDefault(r => r.Name == role.Name);

            if (dbRole == null)
                throw new Exception("Role doesnt exists");

            user.Roles.Clear();
            user.Roles.Add(dbRole);
            user.RegistrationDate = DateTime.Now;

            //todo: remove this hack... cant save user with a null email
            if (user.Email == string.Empty)
            {
                user.Email = "-";
            }

            if (dbRole?.Name == "Manager")
            {
                Cooperative coop = new Cooperative
                {
                    Address = user.Cooperative?.Address,
                    Name = user.Cooperative?.Name
                };

                var existingCoop = _db.Cooperatives.FirstOrDefault(c => c.Address == coop.Address && c.Name == coop.Name);
                user.Cooperative = existingCoop ?? _db.Cooperatives.Add(coop);
            }
            else
            {
                user.Cooperative = null;
            }

            _db.Users.Add(user);

            _db.SaveChanges();
            return true;
        }
    }
}