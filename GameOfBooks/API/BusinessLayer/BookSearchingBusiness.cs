﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Linq;
//using System.Web;
//using API.Models;
//using API.Tools;

//namespace API.BusinessLayer
//{
//    public class BookSearchingBusiness
//    {
//        private readonly GobEntities _db;
//        private readonly string _sessionToken;
//        private readonly User _currentUser;

//        public BookSearchingBusiness(GobEntities db, string sessionToken = "")
//        {
//            _sessionToken = sessionToken;
//            _db = db;
//            _currentUser = UserBusiness.GetCurrentUser(_db, _sessionToken);
//        }

//        public BookInfo[] SearchBookToReceive(BookSearch search)
//        {
//            return SearchBooks(bookCopy => bookCopy.ReceivedDate == null && bookCopy.Canceled != true, search.Value);
//        }

//        public BookInfo[] SearchBookToReserve(BookSearch search)
//        {
//            return
//                SearchBooks(
//                    bookCopy =>
//                        bookCopy.ReceivedDate != null && bookCopy.Canceled != true &&
//                        bookCopy.Reservations.All(b => b.ExpirationDate < DateTime.Now && b.ReceiptDate == null),
//                    search.Value);
//        }

//        public BookInfo[] SearchBookToTransfert(BookSearch search)
//        { 
//            return SearchBooks(bookCopy => bookCopy.Reservations.Any(r =>
//            {
//                var toCoopId = r.ToCooperativeId ?? -1;
//                return r.ExpirationDate > DateTime.Now && r.FromCooperativeId != toCoopId &&
//                       UserBusiness.GetCurrentUser(_db, _sessionToken).CooperativeId == r.FromCooperativeId 
//                       && (r.TransferId == null || ( r.TransferId != null && r.ReceiptDate != null ) );

//            }),
//                search.Value);
//        }

//        public BookInfo[] SearchBookToTransferCoopRequest(BookSearch search) {
//            return SearchBooks(bookCopy => bookCopy.Reservations.Any(r =>
//            {
//                var toCoopId = r.ToCooperativeId ?? -1;
//                var fromCoopId = UserBusiness.GetCurrentUser(_db, _sessionToken).CooperativeId;
//                return r.ExpirationDate > DateTime.Now && r.FromCooperativeId == fromCoopId &&
//                       toCoopId != fromCoopId;
//            }),
//            search.Value);
//        }

//        public BookInfo[] SearchBookToReceiveFromTransfer(BookSearch search)
//        {
//            return SearchBooks(bookCopy => bookCopy.Reservations.Any(r =>
//            {
//                var toCoopId = r.ToCooperativeId ?? -1;
//                var fromCoopId = UserBusiness.GetCurrentUser(_db, _sessionToken).CooperativeId;
//                return r.ExpirationDate > DateTime.Now 
//                       && r.FromCooperativeId == fromCoopId 
//                       && toCoopId != fromCoopId
//                       && r.TransferId != null
//                       && r.Transfer.ReceiptDate == null;
//            }),
//            search.Value);
//        }

//        public BookInfo[] SearchBookToRecover(BookSearch search)
//        {
//            return
//                SearchBooks(
//                    bookCopy =>
//                        bookCopy.Reservations.Any(
//                            r => r.ExpirationDate > DateTime.Now && r.User.Email == search.Value && r.ReceiptDate == null),
//                    search.Value, false);
//        }

//        public BookInfo[] SearchInventory()
//        {
//            return SearchBooks(bookCopy => bookCopy.ReceivedDate != null && bookCopy.CooperativeId == _currentUser.CooperativeId, null, false);
//        }

//        /// <summary>
//        /// Main method of search engine
//        /// </summary>
//        /// <param name="predicate">specific condition for a book search</param>
//        /// <param name="searchTerm"></param>
//        /// <param name="searchTermCondition">Indicates if searchTermConditions have to be applied</param>
//        private BookInfo[] SearchBooks(Func<BookCopy, bool> predicate, string searchTerm, bool searchTermCondition = true)
//        {
//            return _db.BookCopies.Include(b => b.User)
//                .Include(b => b.BookDescription)
//                .Include(b => b.Cooperative)
//                .Include(b => b.Cooperative.BookPrices)
//                .Include(b => b.Condition)
//                .Include(b => b.Ebay)
//                .Include(b => b.Reservations)
//                .Include(b => b.Reservations.Select(r => r.User))
//                .Include(b => b.Reservations.Select(r => r.Transfer))
//                .Where(predicate)
//                .Where(b => !searchTermCondition || SearchTermCondition(b, searchTerm))
//                .Select(b => BookInfoConverter.ToBookInfo(_currentUser, b))
//                .ToArray();
//        }

//        /// <summary>
//        /// Common search conditions
//        /// </summary>
//        /// <param name="bookCopy"></param>
//        /// <param name="searchTerm"></param>
//        /// <returns></returns>
//        private bool SearchTermCondition(BookCopy bookCopy, string searchTerm)
//        {
//            return bookCopy.User?.Email == searchTerm ||
//                   bookCopy.BookDescription.ISNB10 == searchTerm ||
//                   bookCopy.BookDescription.EAN == searchTerm ||
//                   bookCopy.BookDescription.UPC == searchTerm ||
//                   bookCopy.BookDescription.Title.Contains(searchTerm) ||
//                   string.IsNullOrEmpty(searchTerm);
//        }
//    }
//}