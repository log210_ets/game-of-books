﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API.Models;

namespace API.BusinessLayer.BookSearch
{
    public class BookRecoverSearch: BookSearchTemplateMethod
    {
        public BookRecoverSearch(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null) : base(db, sessionToken, searchTerm)
        {
        }

        protected override Func<BookCopy, bool> BuildPredicate()
        {
            return bookCopy =>
                bookCopy.Reservations.Any(
                    r => r.ExpirationDate > DateTime.Now && r.User.Email == SearchTerm && r.ReceiptDate == null);
        }

        protected override bool CompareBookCopyPropertyWithSearchTerm => false;
    }
}