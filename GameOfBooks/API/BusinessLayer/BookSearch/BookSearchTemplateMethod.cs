﻿using System;
using System.Data.Entity;
using System.Linq;
using API.Models;
using API.Tools;

namespace API.BusinessLayer.BookSearch
{
    public abstract class BookSearchTemplateMethod
    {
        protected readonly GobEntities Db;
        protected readonly User CurrentUser;
        protected readonly string SearchTerm;

        protected BookSearchTemplateMethod(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null)
        {
            Db = db;
            CurrentUser = UserBusiness.GetCurrentUser(Db, sessionToken);
            SearchTerm = searchTerm?.Value;
        }

        protected abstract Func<BookCopy, bool> BuildPredicate();
        protected abstract bool CompareBookCopyPropertyWithSearchTerm { get; }

        public BookInfo[] Search()
        {
            return SearchBooks(BuildPredicate(), CompareBookCopyPropertyWithSearchTerm);
        }
        

        /// <summary>
        /// Main method of search engine
        /// </summary>
        /// <param name="predicate">specific condition for a book search</param>
        /// <param name="searchTermCondition">Indicates if searchTermConditions have to be applied</param>
        private BookInfo[] SearchBooks(Func<BookCopy, bool> predicate, bool searchTermCondition = true)
        {
            return Db.BookCopies.Include(b => b.User)
                .Include(b => b.BookDescription)
                .Include(b => b.Cooperative)
                .Include(b => b.Cooperative.BookPrices)
                .Include(b => b.Condition)
                .Include(b => b.Ebay)
                .Include(b => b.Reservations)
                .Include(b => b.Reservations.Select(r => r.User))
                .Include(b => b.Reservations.Select(r => r.Transfer))
                .Where(predicate)
                .Where(b => !searchTermCondition || SearchTermCondition(b, SearchTerm))
                .Select(b => BookInfoConverter.ToBookInfo(CurrentUser, b))
                .ToArray();
        }

        /// <summary>
        /// Common search conditions
        /// </summary>
        /// <param name="bookCopy"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        private bool SearchTermCondition(BookCopy bookCopy, string searchTerm)
        {
            return bookCopy.User?.Email == searchTerm ||
                   bookCopy.BookDescription.ISNB10 == searchTerm ||
                   bookCopy.BookDescription.EAN == searchTerm ||
                   bookCopy.BookDescription.UPC == searchTerm ||
                   bookCopy.BookDescription.Title.Contains(searchTerm) ||
                   string.IsNullOrEmpty(searchTerm);
        }
    }

}