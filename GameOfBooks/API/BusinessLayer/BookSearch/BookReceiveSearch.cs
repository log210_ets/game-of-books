﻿using System;
using API.Models;

namespace API.BusinessLayer.BookSearch
{
    public class BookReceiveSearch: BookSearchTemplateMethod
    {
        public BookReceiveSearch(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null) : base(db, sessionToken, searchTerm)
        {
        }

        protected override Func<BookCopy, bool> BuildPredicate()
        {
            return bookCopy => bookCopy.ReceivedDate == null && bookCopy.Canceled != true;
        }

        protected override bool CompareBookCopyPropertyWithSearchTerm => true;
    }
}