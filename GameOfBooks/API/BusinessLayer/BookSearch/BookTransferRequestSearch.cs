﻿using System;
using System.Linq;
using API.Models;

namespace API.BusinessLayer.BookSearch
{
    public class BookTransferRequestSearch: BookSearchTemplateMethod
    {
        public BookTransferRequestSearch(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null) : base(db, sessionToken, searchTerm)
        {
        }

        protected override Func<BookCopy, bool> BuildPredicate()
        {
            return bookCopy => bookCopy.Reservations.Any(r =>
            {
                var toCoopId = r.ToCooperativeId ?? -1;
                var fromCoopId = CurrentUser.CooperativeId;
                return r.ExpirationDate > DateTime.Now && r.FromCooperativeId == fromCoopId &&
                       toCoopId != fromCoopId;
            });
        }

        protected override bool CompareBookCopyPropertyWithSearchTerm => true;
    }
}