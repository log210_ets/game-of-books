﻿using System;
using System.Linq;
using API.Models;

namespace API.BusinessLayer.BookSearch
{
    public class BookReserveSearch: BookSearchTemplateMethod
    {
        public BookReserveSearch(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null) : base(db, sessionToken, searchTerm)
        {
        }

        protected override Func<BookCopy, bool> BuildPredicate()
        {
            return bookCopy =>
                bookCopy.ReceivedDate != null && bookCopy.Canceled != true &&
                bookCopy.Reservations.All(b => b.ExpirationDate < DateTime.Now && b.ReceiptDate == null);
        }

        protected override bool CompareBookCopyPropertyWithSearchTerm => true;
    }
}