﻿using System;
using System.Linq;
using API.Models;

namespace API.BusinessLayer.BookSearch
{
    public class BookTransferSearch: BookSearchTemplateMethod
    {
        public BookTransferSearch(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null) : base(db, sessionToken, searchTerm)
        {
        }

        protected override Func<BookCopy, bool> BuildPredicate()
        {
            return bookCopy => bookCopy.Reservations.Any(r =>
            {
                var toCoopId = r.ToCooperativeId ?? -1;
                return r.ExpirationDate > DateTime.Now && r.FromCooperativeId != toCoopId &&
                       CurrentUser.CooperativeId == r.FromCooperativeId
                       && (r.TransferId == null || (r.TransferId != null && r.ReceiptDate != null));

            });
        }

        protected override bool CompareBookCopyPropertyWithSearchTerm => true;
    }
}