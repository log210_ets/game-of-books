﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API.Models;

namespace API.BusinessLayer.BookSearch
{
    public class BookInventorySearch: BookSearchTemplateMethod
    {
        public BookInventorySearch(GobEntities db, string sessionToken = "", Models.BookSearch searchTerm = null) : base(db, sessionToken, searchTerm)
        {
        }

        protected override Func<BookCopy, bool> BuildPredicate()
        {
            return bookCopy => bookCopy.ReceivedDate != null && bookCopy.CooperativeId == CurrentUser.CooperativeId;
        }

        protected override bool CompareBookCopyPropertyWithSearchTerm => false;
    }
}