﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Web;
using System.Web.Http;
using API.Amazon;
using API.Attributes;
using API.Models;
using API.Tools;
using Newtonsoft.Json;
using static System.String;
using API.Controllers;

namespace API.BusinessLayer
{
    public class BookBusiness
    {
        private readonly GobEntities _db;
        private readonly string _sessionToken;
        private readonly User _currentUser;

        public BookBusiness(GobEntities db, string sessionToken = "")
        {
            _sessionToken = sessionToken;
            _db = db;
            _currentUser = UserBusiness.GetCurrentUser(_db, _sessionToken);
        }

        public BookCopy GetBookCopy(int id)
        {
            return BookInfoConverter.GetConvertableBookCopies(_db).FirstOrDefault(m => m.BookCopyId == id);
        }

        public BookInfo Save(BookCopy book)
        {
            if (book == null)
                throw new Exception("Book is null");

            if (book.Condition == null)
                throw new Exception("Book condition is null");

            if (book.BookDescription == null)
                throw new Exception("Book description is null");

            var user =
                _db.Users.Include(u => u.Cooperative.BookPrices).FirstOrDefault(u => u.SessionToken == _sessionToken);

            if (user == null)
                throw new Exception("User is not logged in");

            var condition =
                _db.Conditions.FirstOrDefault(
                    c => c.ConditionId == book.Condition.ConditionId || c.BookCondition == book.Condition.BookCondition);

            var description =
                _db.BookDescriptions.FirstOrDefault(
                    c =>
                        c.BookDescriptionId == book.BookDescription.BookDescriptionId &&
                        c.Title == book.BookDescription.Title);

            var cooperative = _db.Cooperatives.FirstOrDefault(c => c.Name == "Unassigned");

            if (cooperative == null)
                throw new Exception("error in default coop assigment, RUN SCRIPT SQL!!!");

            if (condition == null)
                throw new Exception("Book condition isn't valid");

            if (description == null)
                description = _db.BookDescriptions.Add(book.BookDescription);

            _db.SaveChanges();

            var bookCopy = _db.BookCopies.Add(new BookCopy
            {
                User = user,
                BookDescription = description,
                Condition = condition,
                Cooperative = cooperative
            });

            _db.SaveChanges();

            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId));
        }

        public BookInfo EditBookCondition(BookInfo bookInfo)
        {

            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var existingCondition = _db.Conditions.FirstOrDefault(c => c.ConditionId == bookInfo.ConditionId);

            if (existingCondition == null)
                throw new Exception("Condtion is not valid");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Condition).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            bookCopy.Condition = existingCondition;

            _db.SaveChanges();

            bookInfo.BookCondition = bookCopy.Condition.BookCondition;
            bookInfo.ConditionId = bookCopy.ConditionId;

            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId));
        }

        public BookInfo EditCoopPrice(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.BookDescription)
                    .Include(b => b.User)
                    .Include(b => b.Condition)
                    .Include(b => b.Cooperative)
                    .FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Invalid bookCopyId");

            if (bookInfo.CooperativePrice == null)
                throw new Exception("Cooperativce Price must be specified");

            var user = _db.Users.Include(u => u.Cooperative).FirstOrDefault(u => u.SessionToken == _sessionToken);

            if (user == null)
                throw new Exception("User is not logged in");

            if (user.Cooperative == null)
                throw new Exception("User is not in a cooperative, should not be allowed to edit coopPrice");

            var exitstingPrice =
                _db.BookPrices.Any(
                    b => b.CooperativeId == user.CooperativeId && b.BookDescriptionId == bookInfo.BookDescriptionId);

            if (exitstingPrice)
                throw new Exception("Cooperativce is not allowed to change price many times");

            var bookDescription = _db.BookDescriptions.Find(bookInfo.BookDescriptionId);

            if (bookDescription == null)
                throw new Exception("bookDescription is not valid");


            BookPrice bookPrice = new BookPrice
            {
                BookDescription = bookDescription,
                Cooperative = user.Cooperative,
                CoopPrice = bookInfo.CooperativePrice
            };

            _db.BookPrices.Add(bookPrice);
            _db.SaveChanges();

            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId));
        }

        public BookInfo ConfirmReceive(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var user = _currentUser;

            var cooperative = _db.Cooperatives.Find(user.CooperativeId);

            if (cooperative == null)
                throw new Exception("Current user is not attached to a valid cooperative");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Cooperative).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            bookCopy.ReceivedDate = DateTime.Now;
            bookCopy.Cooperative = cooperative;

            bookInfo.CooperativeAddress = cooperative.Address;
            bookInfo.CooperativeName = cooperative.Name;
            bookInfo.ConditionId = cooperative.CooperativeId;

            var reservationDemands = _db.ReservationDemands.Include(d => d.User).Where(b => b.ISNB10 == bookInfo.ISNB10 && b.NotificationDate == null).ToList();
            reservationDemands.ForEach(r => r.NotificationDate = DateTime.Now);

            _db.SaveChanges();

            new Thread(() => PerfomReservationNotification(reservationDemands, bookInfo)).Start();
            new Thread(() => new Tools.Email(bookInfo, bookInfo.UserEmail, bookInfo.UserPhoneNumber).SendBookReception()).Start();
            new Thread(() => new Sms(bookInfo, bookInfo.UserEmail, bookInfo.UserPhoneNumber).SendBookReception()).Start();

            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId));
        }

        private void PerfomReservationNotification(List<ReservationDemand> reservationDemands, BookInfo bookInfo)
        {
            foreach (var demand in reservationDemands)
            {
                new Tools.Email(bookInfo, demand.User).SendReservationDemand();
                new Sms(bookInfo, demand.User).SendReservationDemand();
            }
        }

        public BookInfo ConfirmReservation(TransactionBraintree transactionBraintree)
        {
            if (transactionBraintree.bookInfo == null)
                throw new Exception("bookInfo is null");

            var fromCoop = _db.Cooperatives.Find(transactionBraintree.bookInfo.CooperativeId);
            var toCoop = _db.Cooperatives.Find(transactionBraintree.bookInfo.DestinationCooperativeId);

            if (fromCoop == null)
                throw new Exception("The book isn't attached to a valid cooperative.");

            if (toCoop == null && transactionBraintree.bookInfo.DestinationCooperativeId != null)
                throw new Exception("DestinationCooperativeId is not valid");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Cooperative).FirstOrDefault(b => b.BookCopyId == transactionBraintree.bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            

            DateTime? timePlus48 = null;
            if (fromCoop == toCoop || toCoop == null)
                timePlus48 = DateTime.Now.AddHours(48);

            Reservation newlyMadeReservation = new Reservation
            {
                FromCooperativeId = fromCoop.CooperativeId,
                BookCopy = bookCopy,
                ToCooperativeId = toCoop?.CooperativeId,
                ExpirationDate = timePlus48,
                User = _currentUser,
            };
            _db.Reservations.Add(newlyMadeReservation);
            _db.SaveChanges();

            CheckoutController checkoutController = new CheckoutController();
            if (!checkoutController.Transaction((double)BookInfoConverter.CalculateSellingPrice(bookCopy, BookInfoConverter.GetCoopPrice(_currentUser, bookCopy)), newlyMadeReservation, transactionBraintree.nonce))
                throw new Exception("Could not made the payment");


            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId)); ;
        }

        public BookInfo CancelReceive(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var cooperative = _db.Cooperatives.Find(_currentUser.CooperativeId);

            if (cooperative == null)
                throw new Exception("Current user is not attached to a valid cooperative");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Cooperative).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            bookCopy.Canceled = true;
            bookCopy.Cooperative = cooperative;

            bookInfo.CooperativeAddress = cooperative.Address;
            bookInfo.CooperativeName = cooperative.Name;
            bookInfo.ConditionId = cooperative.CooperativeId;

            _db.SaveChanges();

            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId)); 
        }

        public BookInfo ConfirmRecovery(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Cooperative).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            var reservation = _db.Reservations.Find(bookInfo.ReservationId);

            if (reservation == null)
                throw new Exception("reservationId is not valid");

            reservation.ReceiptDate = DateTime.Now;
            reservation.ExpirationDate = DateTime.Now;

            _db.SaveChanges();
            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId));
        }

        public BookInfo CancelRecovery(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Cooperative).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            var reservation =
                _db.Reservations.Include(r => r.User).FirstOrDefault(r => r.ReservationId == bookInfo.ReservationId);

            if (reservation == null)
                throw new Exception("reservationId is not valid");

            reservation.ExpirationDate = DateTime.Now;

            _db.SaveChanges();

            new Tools.Email(bookInfo, reservation.User).SendCancelTransaction();
            new Sms(bookInfo, reservation.User).SendCancelTransaction();

            return BookInfoConverter.ToBookInfo(_currentUser, _db.BookCopies.Find(bookCopy.BookCopyId));
        }

        public BookInfo AddToEbay(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(x => x.Ebay).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            if (bookCopy.Ebay != null)
                throw new Exception("Book is already on eBay");

            bookCopy = EbaySDK.EbaySDK.AddItem((double)BookInfoConverter.CalculateSellingPrice(bookCopy, BookInfoConverter.GetCoopPrice(_currentUser, bookCopy)), bookCopy.BookCopyId);

            return BookInfoConverter.ToBookInfo(_currentUser, bookCopy);
        }

        public BookInfo RemoveFromEbay(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");
            
            var bookCopy =
                BookInfoConverter.GetConvertableBookCopies(_db).Include(b => b.Ebay).FirstOrDefault(b => b.BookCopyId == bookInfo.BookCopyId);

            if (bookCopy == null)
                throw new Exception("Book copy id is not valid");

            if (bookCopy.Ebay == null)
                throw new Exception("Book is not on ebay");

            bookCopy = EbaySDK.EbaySDK.RemoveItem(bookCopy.Ebay.EbayItemID);

            return BookInfoConverter.ToBookInfo(_currentUser, bookCopy);
        }

        public BookDescription SearchBookDescription(Models.BookSearch search)
        {
            if (IsNullOrWhiteSpace(search.Value))
                return new BookDescription();
            
            search.Value = search.Value.Replace("-", Empty).Trim();

            // Look for a bookDescription
            var bookDescription = _db.BookDescriptions.FirstOrDefault(c => c.EAN == search.Value ||
                c.ISNB10 == search.Value ||
                c.UPC == search.Value);

            //If no result look on amazon
            if (bookDescription == null)
            {
                AmazonSearch amazon = new AmazonSearch();
                bookDescription = amazon.ItemLookupRequest(search.Value);


                if (bookDescription != null)
                {
                    // Searh again in datebase in case it already existe
                    var existingBookDescription = _db.BookDescriptions.FirstOrDefault(c => c.EAN == bookDescription.EAN);

                    if (existingBookDescription == null)
                    {
                        _db.BookDescriptions.Add(bookDescription);
                        _db.SaveChanges();
                    }
                    else
                    {
                        bookDescription = existingBookDescription;
                    }

                }
            }

            //todo LazyLoading is disabled but still load BookPrices...cause a referencing loop
            if (bookDescription != null)
            {
                bookDescription.BookPrices = null;
            }

            return bookDescription;
        }

        public string RequestNotification(BookInfo bookInfo)
        {
            if (bookInfo == null)
                throw new Exception("bookInfo is null");

            if (string.IsNullOrEmpty(bookInfo.ISNB10))
                throw new Exception("ISBN10 is null");

            if (_db.ReservationDemands.Count(r => r.ISNB10 == bookInfo.ISNB10 && r.UserId == _currentUser.UserId) == 0)
            {
                _db.ReservationDemands.Add(new ReservationDemand
                {
                    ISNB10 = bookInfo.ISNB10,
                    User = _currentUser
                });

                _db.SaveChanges();
            }
            return _currentUser.Email;
        }
    }
}