﻿
using API.Models;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data.Entity;
using System.Collections.Generic;
using System;
using System.Web.Script.Services;
using System.Web.Services;
using API.Attributes;
using Newtonsoft.Json;
using API.BusinessLayer;
using System.Threading;
using API.Tools;

namespace API.BusinessLayer
{
    public class TransfertBusiness
    {
        private readonly GobEntities _db;
        private readonly string _sessionToken;
        private readonly User _currentUser;

        public TransfertBusiness(GobEntities db, string sessionToken = "")
        {
            _sessionToken = sessionToken;
            _db = db;
            _currentUser = UserBusiness.GetCurrentUser(_db, _sessionToken);
        }



        // UC07
        public BookInfo[] SearchBookToTransfer()
        {
            // Search for all the pending reservation
            var reservations = _db.Reservations
                .Include(c => c.Transfer)
                .Include(c => c.BookCopy)
                .Include(c => c.BookCopy.BookDescription)
                .Include(c => c.BookCopy.Condition)
                .Include(c => c.BookCopy.User)
                .Include(c => c.User)
                .Include(c => c.Cooperative1)
            .Where(c =>
               c.FromCooperativeId == _currentUser.CooperativeId &&
               c.ToCooperativeId != _currentUser.CooperativeId &&
               (c.ReceiptDate == null && c.Transfer == null))
               .ToList().OrderBy(c => c.ToCooperativeId);


            var booksInfos = reservations.ToList().Select(r =>
                BookInfoConverter.ToBookInfo(_currentUser, r.BookCopy, r)
                ).ToArray();

            return booksInfos;
        }



        public void CreateTransfer(Reservations reservations)
        {
            if (reservations == null)
                throw new Exception("reservations not found");


            List<Reservation> lstReservations = new List<Reservation>();

            foreach (var reservation in reservations.reservations)
            {
                var newReservation = _db.Reservations.FirstOrDefault(c => c.ReservationId == reservation.value &&
                                                                    c.TransferId == null);

                if (newReservation != null)
                    lstReservations.Add(newReservation);
            }

            if(lstReservations.Count == 0)
                throw new Exception("reservations not found");

            // separate all the coopId
            var toCoopId = lstReservations
                .GroupBy(c => c.ToCooperativeId)
                .OrderByDescending(c => c.Count())
                .Select(c => c.Key);


            foreach (var coopId in toCoopId)
            {
                Transfer transfer = new Transfer();
                transfer.ExpeditionDate = DateTime.Now;
                transfer.Reservations = lstReservations.Where(c => c.ToCooperativeId == coopId).ToArray();


                try
                {
                    _db.Transfers.Add(transfer);

                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        // UC08
        public BookInfo[] SearchBookTransfer()
        {

            // Search for all the pending reservation for a coop
            var reservations = _db.Reservations
                .Include(c => c.Transfer)
                .Include(c => c.BookCopy)
                .Include(c => c.BookCopy.BookDescription)
                .Include(c => c.BookCopy.Condition)
                .Include(c => c.User)
                .Include(c => c.Cooperative)
            .Where(c =>
               c.FromCooperativeId != _currentUser.CooperativeId &&
               c.ToCooperativeId == _currentUser.CooperativeId &&
               c.ReceiptDate == null &&
               c.Transfer != null &&
               c.Transfer.ReceiptDate == null)
               .ToList().OrderBy(c => c.TransferId);

            var booksInfos = reservations.ToList().Select(r =>
                BookInfoConverter.ToBookInfo(_currentUser, r.BookCopy, r, r.Transfer)).ToArray();


            return booksInfos;
        }


        // UC08
        public void ConfirmTransfer(Reservations reservations)
        {
            if (reservations == null)
                throw new Exception("reservations not found");

            List<Reservation> lstReservations = new List<Reservation>();

            foreach (var reservationId in reservations.reservations)
            {
                var reservation = _db.Reservations.
                    Include(c => c.Transfer)
                    .Include(c => c.User)
                    .Include(c => c.Cooperative1)
                    .Include(c => c.BookCopy)
                    .Include(c => c.BookCopy.BookDescription)
                    .FirstOrDefault(c => c.ReservationId == reservationId.value &&
                                    c.Transfer.ReceiptDate == null);


                if(reservation != null)
                    lstReservations.Add(reservation);
            }

            if (lstReservations.Count == 0)
                throw new Exception("reservations not found");

            foreach (var reservation in lstReservations)
            {
                if(AllBookReceiceFromTranfer(reservation))
                    reservation.Transfer.ReceiptDate = DateTime.Now;

                reservation.ExpirationDate = DateTime.Now.AddHours(48);


                // the bookCopy is now in an new cooperative
                reservation.BookCopy.Cooperative = reservation.Cooperative1;

                _db.SaveChanges();

                var bookInfo = new BookInfo()
                {
                    Author = reservation.BookCopy.BookDescription.Author,
                    Title = reservation.BookCopy.BookDescription.Title,
                    UserEmail = reservation.User.Email,
                    UserPhoneNumber = reservation.User.PhoneNumber,
                    CooperativeName = reservation.Cooperative1.Name

                };


                new Thread(() => new Tools.Email(bookInfo, bookInfo.UserEmail, bookInfo.UserPhoneNumber).SendTransfertReception()).Start();
                new Thread(() => new Sms(bookInfo, bookInfo.UserEmail, bookInfo.UserPhoneNumber).SendTransfertReception()).Start();
            }

        }

        private bool AllBookReceiceFromTranfer(Reservation resevation)
        {
            bool hasActiveReservation = _db.Transfers
              .Include(c => c.Reservations)
              .Any(c => 
                c.TransferId == resevation.TransferId &&
                
                    c.Reservations.Any(r => r.ExpirationDate == null && r.ReservationId != resevation.ReservationId));


            return !hasActiveReservation;
        }

    }

}
