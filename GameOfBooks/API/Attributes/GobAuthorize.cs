﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using API.Models;

namespace API.Attributes
{
    public class GobAuthorize : AuthorizeAttribute
    {
        private readonly string[] _roles;

        public GobAuthorize(string[] roles)
        {
            _roles = roles;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            bool hasAccess = false;
            var sessionToken = actionContext.Request.Headers.Authorization.ToString();

            GobEntities db = new GobEntities();
            var user = db.Users.Include("Roles").FirstOrDefault(u => u.SessionToken == sessionToken);

            if (user != null)
                hasAccess = user.Roles.Any(r => _roles.Contains(r.Name) || r.Name=="Dev");

            db.Dispose();

            return hasAccess;
        }

        public static User GetLoggedUser(HttpActionContext actionContext)
        {
            var sessionToken = actionContext.Request.Headers.Authorization.ToString();

            GobEntities db = new GobEntities();
            return db.Users.FirstOrDefault(u => u.SessionToken == sessionToken);
        }
    }
}