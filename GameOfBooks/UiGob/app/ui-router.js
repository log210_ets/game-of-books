﻿(function () {
    'use strict';

    var app = angular.module('appGob');

    app.config(function ($stateProvider) {

        $stateProvider.state('login',
            {
                url: '/login',
                templateUrl: 'app/components/user/loginUser.html',
                controller: 'UserController'
            })
            .state('gob',
            {
                url: '/game-of-books',
                controller: 'LayoutController',
                templateUrl: 'app/components/shared/layout.html'
            })
            .state('bookAdd',
            {
                url: '/book-add',
                templateUrl: 'app/components/book/bookAdd.html',
                data: { access: 'book_add' }
            })
            .state('bookReceive',
            {
                url: '/book-receive',
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html',
                data: { access: 'book_receive' },
                params: { config: 'bookReceivingList' }
            })
            .state('bookReserve',
            {
                url: '/book-reserve',
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html',
                data: { access: 'book_reserve' },
                params: { config: 'bookReserveList' }
            })
            .state('bookTransfer',
            {
                url: '/book-transfer',
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html',
                data: { access: 'book_transfer' },
                params: { config: 'bookToTransfer' }
            })
            .state('bookRecover',
            {
                url: '/book-recover',
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html',
                data: { access: 'book_recover' },
                params: { config: 'bookToRecover' }
            })
            .state('bookInventory',
            {
                url: '/inventory',
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html',
                data: { access: 'book_inventory' },
                params: { config: 'bookInventory' }
            })
            .state('bookFromTransfer',
            {
                url: '/book-rec-transfer',
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html',
                data: { access: 'book_rec_transfer' },
                params: { config: 'bookFromTransfer' }
            });
    });
})();