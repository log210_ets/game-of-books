﻿(function() {
    'use strict';

    var app = angular.module('appGob',
    ['angular-loading-bar', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'LocalStorageModule', 'ui.router', 'ngResource', 'ngAnimate', 'toastr', 'pascalprecht.translate', 'angular.filter']);

    app.config(function($httpProvider, localStorageServiceProvider, $stateProvider, $provide) {
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = false;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];
        $httpProvider.defaults.headers.common["Content-Type"] = "text/html";

        $httpProvider.interceptors.push(function ($q, localStorageService) {
            return {
                'request': function (config) {
                    $httpProvider.defaults.headers.post['Authorization'] = localStorageService.get("sessionToken");
                    return config;
                }
            };
        });

        localStorageServiceProvider
            .setPrefix('appGob')
            .setStorageType('sessionStorage')
            .setNotify(true, true);

        $stateProvider.decorator('data',
            function(state, parent) {
                var stateData = parent(state);
                if (stateData) {
                    if (stateData.access) {
                        state.resolve.security = [
                            '$q', 'userSettings', function ($q, userSettings) {
                                if (!userSettings.isGranted(stateData.access)) {
                                    return $q.reject("Not Authorized");
                                }
                            }
                        ];
                    }
                }
                return stateData;
            }
        );

        $stateProvider.decorator('params', function (state, parent) {
            var params = parent(state);
            return params;
        });
    });


    app.config(function (toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-top-center',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body',
            timeOut: 5000,
            closeButton: true
        });
    });

    app.constant('apiUrl',
    {
        url: 'http://api.localhost:8081/api/'
    });

    app.config(function ($translateProvider) {
        //$translateProvider.useSanitizeValueStrategy('escape');
        $translateProvider.preferredLanguage('fr');
        $translateProvider.useLoader('asyncLoader');
    });

    app.factory('asyncLoader', function ($q, $timeout, $http, apiUrl) {

        return function (options) {
            var baseUrl = apiUrl.url + 'translate';

            var deferred = $q.defer(),
                translations;

            $http.cache = true;
            $http.get(baseUrl)
                .success(function(data) {
                    console.log(data);
                    translations = {};
                    if (options.key === 'en') {
                        angular.forEach(data,
                            function(translation) {
                                translations[translation.StringKey] = translation.StringEn;
                            });
                    } else {
                        angular.forEach(data,
                            function(translation) {
                                translations[translation.StringKey] = translation.StringFr;
                            });
                    }

                    deferred.resolve(translations);
                });

            return deferred.promise;
        };
    });

    app.filter('textfilter', [function () {
        return function (input) {
            if (arguments.length > 1) {
                // If we have more than one argument (insertion values have been given)
                var str = input;
                // Loop through the values we have been given to insert
                for (var i = 1; i < arguments.length; i++) {
                    // Compile a new Regular expression looking for {0}, {1} etc in the input string
                    var reg = new RegExp("\\{" + (i - 1) + "\\}");
                    // Perform the replace with the compiled RegEx and the value
                    str = str.replace(reg, arguments[i]);
                }
                return str;
            }

            return input;
        };
    }]);

    app.run(function ($state, userService) {
        userService.isUserLoggedIn()
            .then(function (result) {
                if (result.data === "true" && $state.$current.self.name !== "") {
                    $state.go($state.$current.self.name);
                } else {
                    $state.go('login');
                }
            });
    });
})();