﻿(function () {
    'use strict';
    angular
        .module('appGob')
        .controller('LayoutController',
        [
            '$scope', 'userSettings', '$state', 'mobileSettings', function ($scope, userSettings, $state, mobileSettings) {

                var vm = this;
                vm.scope = $scope;
                vm.userSettings = userSettings;
                vm.isMobile =  mobileSettings.isMobile;
            }
        ]);
})();