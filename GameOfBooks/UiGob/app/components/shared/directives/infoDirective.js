﻿angular.module("appGob")
    .directive("infoSummary",
        function() {
            return {
                restrict: "E",
                scope: {
                    dataObject: '@dataObject',
                    text: "@text"
                },
                template:
                    '<p>{{text}}</p>' +
                    '<p>{{dataObject}}</p>' +
                        ' <dl ng-repeat="(key, value) in dataObject" class="dl-horizontal">       ' +
                        ' <dt>{{key}}</dt>              ' +
                        ' <dd>{{value}}</dd>                     ' +
                        ' </dl>                            '
            };
        });

angular.module("appGob")
    .directive("bookInfoList",
        function () {
            return {
                restrict: "E",
                scope: {
                    bookInfoes: '=',
                    config: '='
                },
                templateUrl: 'app/components/book/bookInfoList/bookInfoList.html'
            };
        });