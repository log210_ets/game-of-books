﻿angular.module("appGob")
    .directive("missingInfoAlert",
        function($translate) {
            return {
                restrict: "E",
                //todo link controller scope to excute instant translation
                template: "<div uib-alert ng-class=\"'alert-info'\">" + $translate.instant('MISSING_INFO') + "</div>"
            };
        });