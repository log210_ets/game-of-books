﻿(function () {
    'use strict';
    angular
        .module('appGob')
        .controller('HeaderController',
        [
            '$scope', 'userSettings', '$translate', 'mobileSettings', function ($scope, userSettings, $translate, mobileSettings) {

                var vm = this;
                vm.scope = $scope;
                vm.userSettings = userSettings;
                vm.isMobile = mobileSettings.isMobile;

                vm.otherLanguage = function() {
                    if ($translate.use() === 'en') {
                        return "FR";
                    } else {
                        return "EN";
                    }
                };

                vm.changeLanguage = function () {
                    if ($translate.use() === 'en') {
                        $translate.use('fr');
                    } else {
                        $translate.use('en');
                    }
                };

            }
        ]);
})();