﻿angular.module('appGob')
    .factory('userSettings',
    [
        '$http', 'localStorageService',
        function($http, localStorageService) {

            var vm = this;
            var roleAccesses =
            [
                { access: "header_book", roles: ["Manager", "Student"] },
                { access: "header_logout", roles: ["Manager", "Student"] },
                { access: "book_add", roles: ["Student"] },

                { access: "book_receive", roles: ["Manager"] },
                { access: "book_reserve", roles: ["Student"] },
                { access: "book_recover", roles: ["Manager"] },
                { access: "book_transfer", roles: ["Manager"] },
                { access: "book_inventory", roles: ["Manager"] },
                { access: "book_rec_transfer", roles: ["Manager"] }
            ];

            return {
                user: vm.user,
               
                isGranted: function(roleAccess) {
                    var isGranted = false;
                    var userRoles = localStorageService.get("roles");
                    var sessionToken = localStorageService.get("sessionToken");

                    if (sessionToken !== null && typeof sessionToken !== 'undefined') {
                        if (roleAccess && !_.some(roleAccesses, function(r) { return r.access === roleAccess })) {
                            throw "roleAccess is not defined in roleAccesses";
                        } else {

                            var roleAccessResult = _.find(roleAccesses, function(r) { return r.access === roleAccess });

                            isGranted = _.some(roleAccessResult.roles,
                                function(role) {
                                    return _.some(userRoles,
                                        function(userRole) {
                                            return role === userRole || userRole === 'Dev';
                                        });
                                });
                        }
                    }

                    return isGranted;
                }
            };
        }
    ]);