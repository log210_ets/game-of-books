﻿(function() {
    'use strict';

    angular.module('appGob')
    .controller('ModalInstanceCtrl',
        function($uibModalInstance) {
            var vm = this;

            vm.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        });
})();
