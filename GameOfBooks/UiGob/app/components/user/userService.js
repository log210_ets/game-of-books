﻿angular.module('appGob')
    .service('userService',
    [
        '$http', 'apiUrl', 'localStorageService', function ($http, apiUrl, localStorageService) {

            var baseUrl = apiUrl.url + 'user/';

            //$http.defaults.headers.post['access-token'] = localStorageService.get("sessionToken");


            this.login = function (user) {
                var parameter = JSON.stringify(user);
                var promise = $http.post(baseUrl + 'login', parameter).
                success(function (data, status, headers, config) {
                    var result = angular.fromJson(data);
                    localStorageService.set("sessionToken", result.SessionToken);
                    localStorageService.set("roles", result.Roles);
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });
                return promise;
            }

            this.save = function (user) {
                var parameter = JSON.stringify(user);
                var promise = $http.post(baseUrl + 'save', parameter).
                success(function (data, status, headers, config) {
                    console.log(data);
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });
                return promise;
            };

            this.emailAvailable = function (email) {
                $http.defaults.headers.common['Authorization'] = localStorageService.get("sessionToken");
                var parameter = JSON.stringify(email);

                var promise = $http.post(baseUrl + 'emailAvailable', email).
                success(function (data, status, headers, config) {
                    console.log(data);
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });

                return promise;
            };

            this.isUserLoggedIn = function() {
                $http.defaults.headers.common['Authorization'] = localStorageService.get("sessionToken");

                var promise = $http.post(baseUrl + 'isUserLoggedIn')
                    .success(function(data, status, headers, config) {
                        console.log(data);
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });

                return promise;
            };

            this.logout = function () {
                $http.defaults.headers.common['Authorization'] = localStorageService.get("sessionToken");

                var promise = $http.post(baseUrl + 'logout')
                    .success(function (data, status, headers, config) {
                        localStorageService.clearAll();
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });

                return promise;
            };
        }
    ]);