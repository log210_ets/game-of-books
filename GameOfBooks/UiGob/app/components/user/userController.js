﻿(function () {
    'use strict';
    angular
        .module('appGob')
        .controller('UserController',
        [
            '$scope', 'userService', 'userSettings', 'localStorageService', '$uibModal', '$log', '$state',
            function($scope, userService, userSettings, localStorageService, $uibModal, $log, $state) {

                var vm = this;
                vm.scope = $scope;
                vm.isEmailAvailable = false;
                vm.registrationCompleted = false;
                vm.isLoading = false;
                vm.user = {
                    "email": "",
                    "password": "",
                    "phoneNumber": "",
                    "cooperative": {
                        "name": "",
                        "address": ""
                    },
                    "roles": [
                        {
                            "name": ""
                        }
                    ],
                    "registrationDate": ""
                };
                vm.userSettings = userSettings;

                vm.emailAvailable = function() {

                    var email = vm.user.email;

                    if (email) {
                        var result = userService.emailAvailable({ "email": email })
                        .then(function(result) {
                            vm.isEmailAvailable = result.data;
                        });
                    } else
                        vm.isEmailAvailable = null;

                }

                vm.save = function(form) {
                    if (form.$valid) {
                        vm.isLoading = true;
                        var result = userService.save(vm.user)
                        .then(function(result) {
                            vm.isLoading = false;
                            vm.registrationCompleted = true;
                        });
                    }
                }

                vm.openRegisterForm = function(size) {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        ariaLabelledBy: 'Register Form',
                        ariaDescribedBy: 'Register Form Content',
                        templateUrl: 'app/components/user/editUser.html',
                        controller: 'ModalInstanceCtrl',
                        controllerAs: 'vm',
                        size: 'md',
                        scope: $scope,
                        resolve: {
                            items: function() {
                                return vm.items;
                            }
                        }
                    });

                    modalInstance.result.then(function() {
                    },
                    function() {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                };

                vm.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                };

                vm.login = function(form) {
                    if (form.$valid) {
                        vm.isLoading = true;
                        userService.login(vm.user)
                            .then(function(result) {
                                vm.isLoading = false;
                                var user = angular.fromJson(result.data);
                                if (user.IsAuthentified === true) {
                                    if (_.some(user.Roles, function(r) { return r === 'Student' })) {
                                        $state.go('bookAdd');
                                    } else {
                                        $state.go('gob');
                                    }
                                }
                            });
                    }
                };

                vm.logout = function () {
                    vm.isLoading = true;
                    userService.logout()
                           .then(function (result) {
                               vm.isLoading = false;
                               $state.go('login');
                           });
                }

                vm.test = function () {
                    var result = localStorageService.get("sessionToken");
                    console.log(result);
                }
            }
        ]);
})();