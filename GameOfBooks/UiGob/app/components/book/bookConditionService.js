﻿angular.module('appGob')
    .service('bookConditionService',
    [
        '$http', 'apiUrl', function ($http, apiUrl) {

            var baseUrl = apiUrl.url + 'bookCondition';

            this.getAllConditions = function () {
                var promise = $http.get(baseUrl).
                success(function (data, status, headers, config) {
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });
                return promise;
            }
        }
    ]);