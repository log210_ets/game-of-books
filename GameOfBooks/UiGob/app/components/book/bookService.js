﻿angular.module('appGob')
    .service('bookService',
    [
        '$http', '$q', 'apiUrl', function($http, $q, apiUrl) {

            var baseUrl = apiUrl.url + 'book/';
            var transferUrl = apiUrl.url + 'transfer/';
            var checkoutUrl = apiUrl.url + 'checkout/';
     

            this.searchBook = function(bookSearch) {

                var parameter = JSON.stringify(bookSearch);
                var promise = $http.post(baseUrl + 'searchBook', parameter)
                    .success(function(data, status, headers, config) {
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.search = function(action, bookSearch) {

                var parameter = bookSearch ? JSON.stringify(bookSearch) : null;
                var promise = $http.post(baseUrl + action, parameter)
                    .success(function(data, status, headers, config) {
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.initialize = function (action) {

                var promise = $http.post(baseUrl + action)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.editBookCondition = function(bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'editBookCondition', parameter)
                    .success(function(data, status, headers, config) {
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.requestNotification = function (bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'requestNotification', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.editBookPrice = function(bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'editCoopPrice', parameter)
                    .success(function(data, status, headers, config) {
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }
            
            this.confirmReceive = function (bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'confirmReceive', parameter)
                    .success(function(data, status, headers, config) {
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.cancelReceive = function (bookInfo) {
                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'cancelReceive', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.confirmRecovery = function (bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'confirmRecovery', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.cancelRecovery = function (bookInfo) {
                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'cancelRecovery', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.addToEbay = function (bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'addToEbay', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.removeFromEbay = function (bookInfo) {

                var parameter = JSON.stringify(bookInfo);
                var promise = $http.post(baseUrl + 'removeFromEbay', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;

            }

            this.confirmTransfer = function (reservations) {
                var parameter = JSON.stringify(reservations);
                var promise = $http.post(baseUrl + 'confirmTransfer', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.createTransfer = function (reservations) {
                
                var parameter = JSON.stringify(reservations);
                var promise = $http.post(baseUrl + 'createTransfer', parameter)
                    .success(function (data, status, headers, config) {
                        return data;
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }

            this.getCheckoutToken = function () {
                var promise = $http.get(checkoutUrl + 'get').
                success(function (data, status, headers, config) {
                    return data;
                }).
                error(function (date, status, headers, config) {
                    console.log(data);
                    return true;
                })
                return promise;
            }

            this.confirmReservation = function (bookInfo, nonce) {
                var obj = {};
                obj.bookInfo = bookInfo;
                obj.nonce = nonce;
                var parameter = JSON.stringify(obj);
                console.log(parameter);
                var promise = $http.post(baseUrl + 'confirmReservation', parameter).
                success(function (data, status, headers, config) {
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });
                return promise;
            }

            this.addBookToSell = function(bookCopy) {

                var parameter = JSON.stringify(bookCopy);
                var promise = $http.post(baseUrl + 'save', parameter)
                    .success(function(data, status, headers, config) {
                        return data;
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        return true;
                    });
                return promise;
            }
        }
    ]);