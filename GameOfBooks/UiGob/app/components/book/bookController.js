﻿(function () {
    'use strict';
    angular
        .module('appGob')
        .controller('BookController',
        [
            '$scope','userSettings', 'bookService', 'bookConditionService', '$translate',
            function ($scope, userSettings, bookService, bookConditionService, $translate) {

                var vm = this;
                vm.bookCode = null;
                vm.scope = $scope;
                vm.bookDescription = null;
                vm.isLoading = false;
                vm.bookConditions = [];
                vm.searchResult = {};
                vm.searchButtonClicked = false;
                vm.bookCopyInfo = null;
                vm.currentLanguage = $translate.use();

                vm.getCurrentLanguage = function() {
                    return $translate.use();
                };

                vm.conditions = function () {
                    var conditions = [];
                    angular.forEach(vm.bookConditions,
                        function(cond) {
                            if ($translate.use() === 'en') {
                                conditions.push({"ConditionId" : cond.ConditionId, "BookCondition" :cond.BookCondition });
                            } else {
                                conditions.push({ "ConditionId": cond.ConditionId, "BookCondition": cond.BookConditionFr });
                            }
                        });
                    return conditions;
                };

                vm.searchBook = function (form) {
                    vm.bookCopyInfo = null;
                    if (form.$valid) {
                        vm.isLoading = true;
                        bookService.searchBook({ value: vm.bookCode })
                            .then(function(result) {

                                var bookDescription = angular.fromJson(result.data);
                                if (bookDescription) {
                                    vm.searchResult = {};
                                    angular.copy(bookDescription, vm.searchResult);
                                } else {
                                    vm.searchResult = null;
                                }
                                vm.searchButtonClicked = true;

                                vm.bookDescription = bookDescription;
                            });

                        vm.isLoading = false;
                    }
                }

                vm.addBookToSell = function(form) {
                    if (form.$valid) {
                        var bookCopy = {
                            "BookDescription": vm.bookDescription,
                            "Condition": angular.fromJson(vm.bookCondition)
                        };
                        bookService.addBookToSell(bookCopy).then(function (result) {
                            vm.bookCopyInfo = angular.fromJson(result.data);
                        });
                    }
                }

                vm.fieldDisabled = function(field) {
                    if (!vm.searchResult)
                        return false;

                    if (vm.searchResult[field] === "" || vm.searchResult[field] === null) {
                        return false;
                    }

                    return true;
                }

                vm.init = function () {
                    bookConditionService.getAllConditions()
                        .then(function (result) {
                            vm.bookConditions = angular.fromJson(result.data);
                        });
                }

                vm.init();
            }
        ]);
})();