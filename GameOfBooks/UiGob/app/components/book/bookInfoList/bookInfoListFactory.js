﻿angular.module('appGob')
    .factory('bookInfoListFactory',
        function() {
            // BookCopyId
            // UserId
            // UserEmail
            // BookDescriptionId
            // ISNB10
            // EAN
            // UPC
            // Title
            // Author
            // NbPage
            // DefaultPrice
            // CooperativeId
            // CooperativeName
            // CooperativeAddress
            // CooperativePrice
            // ConditionId
            // BookCondition
            // SellingPrice


            return {
                configs: {
                    bookReceivingList: {
                        title: "RECEIVE_BOOKS",
                        properties: [
                            "UserEmail",
                            "BookCondition",
                            "ISNB10",
                            "Title",
                            "SellingPrice",
                            "DefaultPrice",
                            "CooperativePrice"
                        ],
                        editableProperties: [
                            "BookCondition",
                            "CooperativePrice"
                        ],
                        actions: [
                            "ConfirmReceive",
                            "CancelReceive"
                        ],
                        searchAction: "searchBookToReceive"
                    },

                    bookReserveList: {
                        title: "RESERVE_BOOKS",
                        properties: [
                            "ISNB10",
                            "Title",
                            "Author",
                            "CooperativeName",
                            "BookCondition",
                            "DefaultPrice",
                            "CooperativePrice",
                            "SellingPrice"
                        ],

                        editableProperties: [
                            "CooperativeName"
                        ],

                        actions: [
                            "ConfirmReservation",
                            "RequestNotification"
                        ],
                        searchAction: "searchBookToReserve"
                    },

                    bookToTransfer : {
                        title: "TRANSFER_BOOKS",
                        properties: [
                            "ISNB10",
                            "Title",
                            "Author",
                            "CooperativeName",
                            "BookCondition",
                            "SellingPrice"
                        ],
                        selectable: true,
                        actions: [
                            "createTransfer"
                        ],

                        initializeAction: "searchBookToTransfer"
                    },

                    bookFromTransfer: {
                        title: "RECEIVE_TRANSFER",
                        properties: [
                            "ISNB10",
                            "Title",
                            "Author",
                            "CooperativeName",
                            "BookCondition",
                            "SellingPrice"
                        ],
                        selectable: true,
                        groupBy: "TransfertId",
                        actions: ["confirmTransfer"],
                        initializeAction: "searchTransfer"
                    },

                    bookToRecover: {
                        title: "RECOVER_BOOKS",
                        properties: [
                            "ISNB10",
                            "Title",
                            "Author",
                            "CooperativeName",
                            "BookCondition",
                            "SellingPrice"
                        ],
                        actions: [
                            "ConfirmRecovery",
                            "CancelRecovery"
                        ],
                        searchAction: "searchBookToRecover"
                    },

                    bookInventory: {
                        title: "INVENTORY",
                        properties: [
                            "ISNB10",
                            "Title",
                            "Author",
                            "CooperativeName",
                            "BookCondition",
                            "SellingPrice",
                            "EbayAddedDate"
                        ],
                        actions: [
                            "AddToEbay",
                            "RemoveFromEbay"
                        ],
                        initializeAction: "searchInventory"
                    }
                }
            };
        });