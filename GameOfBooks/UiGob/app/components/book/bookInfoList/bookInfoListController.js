﻿(function () {
    'use strict';
    angular
        .module('appGob')
        .controller('BookInfoListController',
        [
            '$scope', 'bookService', 'bookConditionService', 'cooperativeService', 'toastr', 'bookInfoListFactory', '$stateParams', '$uibModal', '$log', '$translate', '$filter', 'mobileSettings',
            function ($scope, bookService, bookConditionService, cooperativeService, toastr, bookInfoListFactory, $stateParams, $uibModal, $log, $translate, $filter, mobileSettings) {

                var vm = this;
                var modalInstance = null;
                vm.isLoading = false;
                vm.searchResults = null;
                vm.searchResultsSelected = [];
                vm.config = null;
                vm.bookConditions = [];
                vm.cooperatives = [];
                vm.isMobile = mobileSettings.isMobile;

                vm.getCurrentLanguage = function () {
                    return $translate.use();
                };

                vm.getBookCondition = function(bookInfo) {
                    var condition = _.find(vm.bookConditions, function (cond) { return cond.ConditionId === bookInfo.ConditionId });
                    return $translate.use() === 'en' ? condition.BookCondition : condition.BookConditionFr;
                };

                vm.hasProperty = function(prop) {
                    return _.contains(vm.config.properties, prop);
                };

                vm.hasEditableProperty = function (prop) {
                    return _.contains(vm.config.editableProperties, prop);
                };

                vm.hasAction = function (action) {
                    return _.contains(vm.config.actions, action);
                };

                vm.hasSearchAction = function() {
                    return vm.config.searchAction !==  undefined && vm.config.searchAction !== null;
                };

                vm.hasInitializeAction = function () {
                    return vm.config.initializeAction !== undefined && vm.config.initializeAction !== null;
                };

                vm.search = function (form) {
                    vm.bookCopyInfo = null;
                    if (form.$valid) {
                        vm.isLoading = true;
                        vm.searchResults = [];
                        bookService.search(vm.config.searchAction, { value: vm.bookCode })
                            .then(function(result) {
                                angular.forEach(angular.fromJson(result.data),
                                    function(bookInfo) {
                                        vm.calculateSellingPrice(bookInfo);;
                                        bookInfo.FromCooperativeName = bookInfo.CooperativeName;
                                        vm.searchResults.push(bookInfo);
                                    });
                                vm.isLoading = false;

                                if (vm.hasAction("RequestNotification") && vm.searchResults.length === 0) {
                                    vm.openNotificationForm();
                                }
                            });
                    }
                }

                vm.initialize = function () {
                    vm.bookCopyInfo = null;
                    vm.isLoading = true;
                    vm.searchResults = [];
                    bookService.initialize(vm.config.initializeAction)
                            .then(function (result) {
                                angular.forEach(angular.fromJson(result.data),
                                    function (bookInfo) {
                                        vm.calculateSellingPrice(bookInfo);;
                                        bookInfo.FromCooperativeName = bookInfo.CooperativeName;
                                        vm.searchResults.push(bookInfo);
                                    });
                                vm.isLoading = false;
                            });
                };

                vm.requestNotification = function(form) {
                    if (form.$valid && vm.hasAction("RequestNotification")) {
                        vm.isLoading = true;
                        bookService.requestNotification({ ISNB10: vm.notification.ISNB10 })
                            .then(function (result) {
                                var text = $filter('textfilter')($translate.instant('TOAST_REQUEST_NOTIFICATION'),result.data);

                                toastr.success(text);
                                vm.isLoading = false;
                                
                            modalInstance.close();
                            });
                    }
                }

                vm.editBookCondition = function(form, bookInfo) {
                    if (form.$valid) {
                        vm.isLoading = true;

                        bookInfo.Condition = angular.fromJson(vm.bookCondition);
                        bookInfo.ConditionId = bookInfo.Condition.ConditionId;
                        bookService.editBookCondition(bookInfo)
                            .then(function(result) {

                                var editedBookInfo = angular.fromJson(result.data);

                                angular.forEach(vm.searchResults,
                                    function(book) {
                                        if (book.BookCopyId === editedBookInfo.BookCopyId) {
                                            book.BookCondition = editedBookInfo.BookCondition;
                                            book.ConditionId = editedBookInfo.ConditionId;
                                            book.SellingPrice = vm.calculateSellingPrice(book);
                                        }
                                    });

                                vm.editConditionIndex = -1;
                                vm.isLoading = false;
                            });
                    }
                }

                vm.editBookPrice = function(form, bookInfo) {
                    if (form.$valid) {
                        vm.isLoading = true;
                        bookInfo.CooperativePrice = vm.editedBookPrice;

                        bookService.editBookPrice(bookInfo)
                            .then(function(result) {

                                var editedBookInfo = angular.fromJson(result.data);

                                    angular.forEach(vm.searchResults,
                                        function(book) {
                                            if (book.BookDescriptionId === editedBookInfo.BookDescriptionId) {
                                                book.CooperativePrice = editedBookInfo.CooperativePrice;
                                                vm.calculateSellingPrice(book);
                                            }
                                        });

                                    bookInfo = editedBookInfo;
                                vm.editPriceIndex = -1;
                                vm.isLoading = false;
                            });
                    }
                }

                vm.editCooperative = function (form, bookInfo) {

                    if (form.$valid) {
                        vm.isLoading = true;
                        bookInfo.DestinationCooperativeId = vm.Cooperative.CooperativeId === bookInfo.CooperativeId ? null : vm.Cooperative.CooperativeId;
                        bookInfo.CooperativeAddress = vm.Cooperative.Address;
                        bookInfo.CooperativeName = vm.Cooperative.Name;
                        vm.editCooperativeIndex = -1;
                        vm.isLoading = false;
                        vm.calculateSellingPrice(bookInfo);
                    }
                }

                vm.calculateSellingPrice = function(bookInfo) {
                    if (bookInfo.ConditionId !== null) {
                        var supplement = 0;
                        if (bookInfo.DestinationCooperativeId !== undefined &&
                            bookInfo.DestinationCooperativeId !== null &&
                            bookInfo.DestinationCooperativeId !== bookInfo.CooperativeId) {
                            supplement = 10;
                        }

                        var factor = _.find(vm.bookConditions,
                                function(cond) { return cond.ConditionId === bookInfo.ConditionId })
                            .PriceFactor;

                        if (bookInfo.CooperativePrice !== null) {
                            bookInfo.SellingPrice = factor * bookInfo.CooperativePrice + supplement;
                        } else if (bookInfo.DefaultPrice !== null) {
                            bookInfo.SellingPrice = factor * bookInfo.DefaultPrice + supplement;
                        }

                        if (bookInfo.SellingPrice != null) {
                            bookInfo.SellingPrice = bookInfo.SellingPrice.toFixed(2);
                        }

                        return bookInfo.SellingPrice;
                    }
                }

                vm.replaceBookInfo = function(bookInfo) {
                    var index = -1;

                    for (var i = 0; i < vm.searchResults.length; i++) {
                        if (vm.searchResults[i].BookCopyId === bookInfo.BookCopyId) {
                            index = i;
                        }
                    }

                    if (index > -1) {
                        array.splice(index, 1, bookInfo);
                    }
                }


                vm.confirmReceive = function(bookInfo) {
                    vm.isLoading = true;

                    bookService.confirmReceive(bookInfo)
                        .then(function(result) {

                            var editedBookInfo = angular.fromJson(result.data);
                            vm.removeBookFromList(vm.searchResults, editedBookInfo);

                            vm.isLoading = false;

                            var text = $filter('textfilter')($translate.instant('TOAST_RECEIPT_MESSAGE'), bookInfo.UserEmail);
                            toastr.success(text,
                                $translate.instant('TOAST_RECEIPT_TITLE'));
                        });
                }

                vm.cancelReceive = function(bookInfo) {
                    vm.isLoading = true;

                    bookService.cancelReceive(bookInfo)
                        .then(function(result) {

                            //todo show toast
                            var editedBookInfo = angular.fromJson(result.data);
                            vm.removeBookFromList(vm.searchResults,editedBookInfo);
                            vm.isLoading = false;

                            toastr.info($translate.instant('TOAST_SELL_CANCEL'));
                        });
                }

                vm.confirmRecovery = function (bookInfo) {
                    vm.isLoading = true;

                    bookService.confirmRecovery(bookInfo)
                        .then(function (result) {

                            var editedBookInfo = angular.fromJson(result.data);
                            vm.removeBookFromList(vm.searchResults,editedBookInfo);

                            vm.isLoading = false;
                            toastr.success($translate.instant('TOAST_RECOVERY_MESSAGE'), $translate.instant('TOAST_RECOVERY_TITLE'));
                        });
                }

                vm.createTransfer = function () {
                    var params = { "reservations": [] };
                    angular.forEach(vm.searchResultsSelected,
                        function(result) {
                            params.reservations.push({"value": result.ReservationId});
                        });

                    bookService.createTransfer(params)
                        .then(function() {
                            toastr.success($translate.instant('TOAST_TRANSFER_SUCCESS'));
                            angular.forEach(vm.searchResultsSelected,
                                function(selected) {
                                    vm.removeBookFromList(vm.searchResults, selected);
                                });

                            vm.searchResultsSelected = [];
                        });
                }

                vm.confirmTransfer = function () {

                    var params = { "reservations": [] };
                    angular.forEach(vm.searchResultsSelected,
                        function (result) {
                            params.reservations.push({ "value": result.ReservationId });
                        });

                    bookService.confirmTransfer(params)
                        .then(function () {
                            toastr.success($translate.instant('TOAST_TRANSFER_RECEIVE_SUCCESS'));
                            angular.forEach(vm.searchResultsSelected,
                                function (selected) {
                                    vm.removeBookFromList(vm.searchResults, selected);
                                });

                            vm.searchResultsSelected = [];
                        });
                }

                vm.getSelected = function(bookInfo) {
                    var selected = _.find(vm.searchResultsSelected,
                       function (info) {
                           return bookInfo.BookCopyId === info.BookCopyId;
                       });

                    return selected;
                }

                vm.select = function (bookInfo) {
                    var selected = vm.getSelected(bookInfo);
                    if (selected) {
                        vm.removeBookFromList(vm.searchResultsSelected, selected);
                    } else {
                        vm.searchResultsSelected.push(bookInfo);
                    }
                }

                vm.isSelected = function(bookInfo) {
                    var selected = vm.getSelected(bookInfo);

                    return selected !== null && selected !== undefined;
                }

                vm.cancelRecovery = function (bookInfo) {
                    vm.isLoading = true;

                    bookService.cancelRecovery(bookInfo)
                        .then(function (result) {

                            var editedBookInfo = angular.fromJson(result.data);
                            vm.removeBookFromList(vm.searchResults,editedBookInfo);

                            vm.isLoading = false;

                            toastr.info($translate.instant('TOAST_RECOVERY_CANCEL_MESSAGE'), $translate.instant('TOAST_RECOVERY_CANCEL_TITLE'));
                        });
                }

                vm.addToEbay = function (bookInfo) {
                    vm.isLoading = true;
                    bookService.addToEbay(bookInfo)
                        .then(function (result) {

                            var editedBookInfo = angular.fromJson(result.data);
                            bookInfo = editedBookInfo;

                            angular.forEach(vm.searchResults,
                            function (book) {
                                if (book.BookCopyId === editedBookInfo.BookCopyId) {
                                    book.ebayItemID = editedBookInfo.ebayItemID;
                                    book.EbayAddedDate = editedBookInfo.EbayAddedDate;
                                }
                            });

                            if (bookInfo.EbayAddedDate != null) {
                                // Sucess
                                var text = $filter('textfilter')($translate.instant('ADD_EBAY_SUCCESS'));
                                toastr.success(text);
                            }
                            else {
                                //Failure
                                var text = $filter('textfilter')($translate.instant('ADD_EBAY_FAILURE'));
                                toastr.error(text);
                            }

                            vm.isLoading = false;
                        });
                }

                vm.removeFromEbay = function (bookInfo) {
                    vm.isLoading = true;

                    bookService.removeFromEbay(bookInfo)
                        .then(function (result) {
                            var editedBookInfo = angular.fromJson(result.data);

                            angular.forEach(vm.searchResults,
                            function (book) {
                                if (book.BookCopyId === editedBookInfo.BookCopyId) {
                                    book.ebayItemID = editedBookInfo.ebayItemID;
                                    book.EbayAddedDate = editedBookInfo.EbayAddedDate;
                                }
                            });

                            bookInfo = editedBookInfo;


                            if (bookInfo.EbayAddedDate == null) {
                                // Sucess
                                var text = $filter('textfilter')($translate.instant('REMOVE_EBAY_SUCESS'));
                                toastr.success(text);
                            }
                            else {
                                //Failure
                                var text = $filter('textfilter')($translate.instant('REMOVE_EBAY_FAILURE'));
                                toastr.error(text);
                            }

                            vm.isLoading = false;
                        });
                }

                vm.removeBookFromList = function (list, bookInfo) {
                    var index = 0;
                    var indexToRemove = -1;

                    angular.forEach(list,
                        function(book) {
                            if (book.BookCopyId === bookInfo.BookCopyId) {
                                indexToRemove = index;
                            }
                            index++;
                        });

                    if (indexToRemove !== -1) {
                        list.splice(indexToRemove, 1);
                    }
                }

                vm.openNotificationForm = function () {
                    modalInstance = $uibModal.open({
                        animation: true,
                        ariaLabelledBy: 'Notification Form',
                        ariaDescribedBy: 'Notification Form Content',
                        templateUrl: 'app/components/book/bookInfoList/requestNotification.html',
                        controller: 'ModalInstanceCtrl',
                        controllerAs: 'vm',
                        size: 'md',
                        scope: $scope,
                        resolve: {
                            items: function () {
                                return vm.items;
                            }
                        }
                    });

                    modalInstance.result.then(function () {
                    },
                    function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                };

                vm.openBraintreeForm = function (book) {
                    modalInstance = $uibModal.open({
                        animation: true,
                        ariaLabelledBy: 'Notification Form',
                        ariaDescribedBy: 'Notification Form Content',
                        templateUrl: 'app/components/book/bookInfoList/braintree.html',
                        controller: 'ModalInstanceCtrl',
                        controllerAs: 'vm',
                        size: 'md',
                        scope: $scope,
                        resolve: {
                            items: function () {
                                return vm.items;
                            }
                        }
                    });
                     
                    vm.setupBrainTree(book);

                    modalInstance.result.then(function () {
                    },
                    function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                };

                vm.setupBrainTree = function (book) {
                    bookService.getCheckoutToken()
                        .then(function (result) {
                            $scope.book = book;
                            $scope.btoken = result.data;

                            braintree.setup(
                            $scope.btoken,
                            "dropin", {
                                container: "dropin-container",
                                onPaymentMethodReceived: function (obj) {
                                    console.log('Ready for server side');
                                    // Do some logic in here.
                                    // When you're ready to submit the form:
                                    vm.confirmReservation(book, obj.nonce);
                                    vm.cancel();
                                }
                            });
                        });
                }

                vm.confirmReservation = function (bookInfo, nonce) {
                    vm.isLoading = true;

                    bookService.confirmReservation(bookInfo, nonce)
                        .then(function (result) {

                            var editedBookInfo = angular.fromJson(result.data);
                            vm.removeBookFromList(vm.searchResults, editedBookInfo);

                            vm.isLoading = false;

                            toastr.success($translate.instant('TOAST_RESERVE_MESSAGE'), $translate.instant('TOAST_RESERVE_TITLE'));
                        });
                }

                vm.cancel = function () {
                    modalInstance.dismiss('cancel');
                };

                vm.init = function () {
                    vm.config = bookInfoListFactory.configs[$stateParams.config];

                    bookConditionService.getAllConditions()
                        .then(function(result) {
                            vm.bookConditions = angular.fromJson(result.data);
                        });

                    if (vm.hasEditableProperty("CooperativeName")) {
                        cooperativeService.getAllCooperatives()
                            .then(function (result) {
                                vm.cooperatives = angular.fromJson(result.data);
                            });
                    }

                    if (vm.hasInitializeAction()) {
                        vm.initialize();
                    }
                }

                vm.init();
            }
        ]);
})();