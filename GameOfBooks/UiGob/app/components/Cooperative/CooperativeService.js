﻿angular.module('appGob')
    .service('cooperativeService',
    [
        '$http', 'apiUrl', function ($http, apiUrl) {

            var baseUrl = apiUrl.url + 'cooperative';

            this.getAllCooperatives = function () {
                var promise = $http.get(baseUrl).
                success(function (data, status, headers, config) {
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });
                return promise;
            }
        }
    ]);