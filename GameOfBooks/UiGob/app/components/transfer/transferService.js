﻿angular.module('appGob')
    .service('transferService',
    [
        '$http', '$q', 'apiUrl', function ($http, $q, apiUrl) {

            var baseUrl = apiUrl.url + 'transfer/';

            this.searchBook = function (bookSearch) {

                var parameter = JSON.stringify(bookSearch);
                var promise = $http.post(baseUrl + 'searchBook', parameter).
                success(function (data, status, headers, config) {
                    return data;
                }).
                  error(function (data, status, headers, config) {
                      console.log(data);
                      return true;
                  });
                return promise;
            }

          
        }
    ]);