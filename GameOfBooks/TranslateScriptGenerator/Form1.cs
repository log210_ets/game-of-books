﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TranslateScriptGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string file = @"..\..\..\..\Translate.sql";

            string key = txtKey.Text.ToUpper();

            if (File.Exists(file))
            {
                string[] lines = File.ReadAllLines(file, Encoding.UTF8);
                List<string> keys = new List<string>();
                foreach (var line in lines)
                {
                    int t = "IF NOT EXISTS (SELECT TOP 1 * FROM dbo.StringResources WHERE StringKey='".Length;
                    string end = "')BEGIN INSERT INTO";
                    int l = line.IndexOf(end, t, StringComparison.Ordinal);
                    string exKey = line.Substring(t, l - t);
                    keys.Add(exKey);
                }

                if (keys.Contains(key))
                {
                    MessageBox.Show("Key already used in script");
                    return;
                }
            }
            string en = txtEn.Text.Replace("'", "''");
            string fr = txtFr.Text.Replace("'", "''");
            // G:\workspace\game-of-books\game-of-books\GameOfBooks\TranslateScriptGenerator\bin\Debug
            string query =
                $"IF NOT EXISTS (SELECT TOP 1 * FROM dbo.StringResources WHERE StringKey='{key}')BEGIN INSERT INTO dbo.StringResources(StringKey, StringEn, StringFr) VALUES('{key}', '{en}', '{fr}') END";

            File.AppendAllLines(file, new string[] {query}, Encoding.UTF8);

            txtKey.Clear();
            txtEn.Clear();
            txtFr.Clear();
        }
    }
}
